class CoursesController < SecureController
  def list
    subject = Subject.find(params[:subject_id])
    if(!subject)
      render json:{message:"Предмет не найден"},status:501 and return
    end
    render json: subject.courses.map{|course| {
      id:course.id,
      no: course.no,
      name: course.name,
      description:course.description,
      logo:course.logo.url(:medium),
      level:course.level}}
  end
end
