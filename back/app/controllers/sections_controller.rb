class SectionsController < SecureController
  def list
    course = Course.find(params[:course_id])
    progress = current_user.courseProgresses.find_by(course_id:params[:course_id])

    render json: course.sections.order(:no).map{|section| {
      id:section.id,
      no:section.no,
      name: section.name,
      description:section.description,
      scs:section.screencasts.order(:no).map{|sc|{
        id:sc.id,
        no: sc.no,
        name: sc.name,
        logo:sc.logo.url(:thumb),
        video:sc.video.url(),
        free:sc.free
      }}
    }}
  end
  def current 
    course = Course.includes(:sections).find(params[:course_id])
    if(!course)
      render json:{message:"Курс не найден"},status:501 and return
    end
    if(course.sections.size==0)
      render json:{message:"Курс не настроен"},status:501 and return
    end

    progress = current_user.courseProgresses.find_by(course_id:params[:course_id])
    if !progress
      section = course.sections.order(:no).first
      sc=section.screencasts.order(:no).first

      if(!sc)
        render json:{message:"Урок не найден"}, status:501 and return
      end

      progress = current_user.courseProgresses.create(course_id:params[:course_id],section_id:section.id,sc_id:sc.id)
      refreshRedis(progress,section)
    else 
      section = course.sections.find(progress.section_id)
      refreshRedis(progress,section)
    end

    render json: {
      active_id: progress.section_id,
      active_sc_id:progress.sc_id
    }
  end

  def move
    course = Course.find(params[:course_id])
    progress = current_user.courseProgresses.find_by(course_id:params[:course_id])
    if !progress
      render :current and return 
    end 

    section_no = Section.find(progress.section_id).no
    next_section = course.sections.order(:no).find{|section| section.no>section_no}
    sc=next_section.screencasts.order(:no).first

    if(!sc)
      render json:{message:"Урок не найден"}, status:501 and return
    end

    progress.section_id = next_section.id
    progress.sc_id = sc.id
    progress.save
    refreshRedis(progress,next_section)
    render json: {
      active_id: progress.section_id,
      active_sc_id:progress.sc_id
    }
  end

  def activate 
    section = Section.find(params[:section_id])
    if(!section)
      render json:{message:"Раздел не найден"},status:501 and return
    end
    sc = section.screencasts.find(params[:sc_id])
    if(!sc)
      render json:{message:"Урок не найден либо не относится к текущей части"},status:501 and return 
    end

    progress = current_user.courseProgresses.find_by(course_id:section.course_id,section_id:section.id)
    if !progress 
      render json:{message:"Урок не найден либо не относится к текущей части"},status:501 and return 
    end

    progress.sc_id=params[:sc_id]
    progress.save
    render json: {
      active_id: progress.section_id, active_sc_id: progress.sc_id
    }
  end

  private

  def refreshRedis(progress,section)
    if(progress&&progress.section_id)
      redis = Redis.new
      redis.keys("sc:#{current_user.token}:*").each do |key|
        redis.del(key)
      end
      if !progress.completed
        section.screencasts.each do |sc| 
          redis.set("sc:#{current_user.token}:#{sc.id}",1)
        end
      end
    end
  end
end
