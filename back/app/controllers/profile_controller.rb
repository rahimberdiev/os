class ProfileController < SecureController
  def read
    user = User.find(current_user["userid"].to_i)
    if(user)
      render json: {fullname: user.fullname, photo: user.photo.url(:thumb)}
    else
      render json: {message: "user is not found"}
    end
  end

  def write
  end
end
