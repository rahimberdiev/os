class SignoutController < ApplicationController
  def create
    user = User.find_by(username:params[:username])
    render json: {message: "Пользователь с таким именем уже существует"},status:501 and return if user

    user = User.find_by(email:params[:email])
    render json: {message: "Пользователь с такой эл.почтой уже существует"},status:501 and return if user

    user = User.find_by(phone:params[:phone])
    render json: {message: "Пользователь с таким номером тел. уже существует"},status:501 and return if user


    
    user = User.new(username: params[:username], email: params[:email].downcase,fullname:params[:fullname],lang:params[:lang],phone:params[:phone],password: params[:password])
    user.save
    user.token_created_at = DateTime.now
    user.regenerate_token
    render json: {token: user.token, token_created_at: user.token_created_at, email:user.email, phone:user.phone,fullname:user.fullname}
  end
end
