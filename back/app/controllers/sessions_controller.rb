class SessionsController < SecureController
  skip_before_action :require_login, only: [:create,:send_pin,:change_password], raise: false
  def create
    if user= User.valid_login?(params[:username],params[:password])
      clearRedis(user)
      #      PasswordResetMailer.password_reset(user).deliver
      allow_token_to_be_used_only_once_for(user)
      send_auth_token_for_valid_login_of(user)
    else
      render json: {message: "Не верный логин или пароль!"},status:401
    end
  end

  def destroy
    logout
    render json: {message: "Сессия закрыт"},status:200
  end

  private 
  def send_auth_token_for_valid_login_of(user)
    render json: {
      token: user.token,
      token_created_at: user.token_created_at, 
      user_id:user.id,
      username:user.username,
      email:user.email,
      phone:user.phone,
      fullname:user.fullname
    }
  end
  def allow_token_to_be_used_only_once_for(user)
    user.token_created_at = DateTime.now
    user.regenerate_token
    redis = Redis.new
    token = redis.get("user:#{user.id}:token")
    redis.del("token:#{token}") if token

    redis.set("user:#{user.id}:token",user.token)
    redis.set("token:#{user.token}",user.id)
  end
  def clearRedis(user)
    redis = Redis.new
    redis.keys("sc:#{user.token}:*").each do |key|
      redis.del(key)
    end
  end

  def logout
    clearRedis(current_user)
    current_user.invalidate_token
  end 
end
