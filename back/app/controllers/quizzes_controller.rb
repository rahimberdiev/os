class QuizzesController < ApplicationController
  def list
    sc = Screencast.find(params[:sc_id])
    no_ids = sc.quizzes.map{|q| {no:q.no,id:q.id}}
    no_gs = no_ids.group_by{|no_id| no_id[:no]}
    rnd_ids = no_gs.keys.map{|no_g| no_gs[no_g][rand(1..no_gs[no_g].length)-1][:id]}
    quizzes = rnd_ids.map{|rnd_id| sc.quizzes.find{|ex| ex.id == rnd_id}}

    render json: quizzes.map{|q| {
      id:q.id, 
      no:q.no, 
      question: q.question, 
      score:q.score,
      options: q.options.map{|o| {id: o.id, value: o.value}}, 
      imgs: q.imgs.order(:title).map{|img| {
        title: img.title,
        img: img.img.url(:medium)
      }}
    }}
  end
  def check
    sc = Screencast.includes(:quizzes).find(params[:sc_id])
    corrects = params[:work].count{|w| 
      sc.quizzes.find{|ex| 
        ex.id == w[:id] && ex.options.find{|ans| ans.id==w[:ans] && ans.correct}
      }
    }
    render json: {
      total:params[:work].length,
      corrects:corrects,
      score:(corrects*100/params[:work].length)
    }
  end
end
