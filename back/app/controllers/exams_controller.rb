class ExamsController < ApplicationController
  def list
    section = Section.find(params[:section_id])
    no_ids = section.exams.map{|q| {no:q.no,id:q.id}}
    no_gs = no_ids.group_by{|no_id| no_id[:no]}
    rnd_ids = no_gs.keys.map{|no_g| no_gs[no_g][rand(1..no_gs[no_g].length)-1][:id]}
    exams = rnd_ids.map{|rnd_id| section.exams.find{|ex| ex.id == rnd_id}}

    render json: exams.map{|q| {
      no:q.no,
      id:q.id, 
      question: q.question, 
      score:q.score,
      options: q.answers.map{|o| {id: o.id, value: o.value}}, 
      pics: q.pics.order(:title).map{|pic| {
        title: pic.title,
        img: pic.img.url(:medium)
      }}
    }}
  end
  def check
    section = Section.includes(:exams).find(params[:section_id])
    corrects = params[:work].count{|w| 
      section.exams.find{|ex| 
        ex.id == w[:id] && ex.answers.find{|ans| ans.id==w[:ans] && ans.correct}
      }
    }
    render json: {
      total:params[:work].length,
      corrects:corrects,
      score: corrects*100/params[:work].length, 
      pass:(corrects*100/params[:work].length) > 85 ? 1 : 0
    }
  end
end
