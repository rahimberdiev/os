class ScreencastsController < SecureController
  def list
    section= Section.find(params[:section_id])
    if(!section)
      render json:{message:"Раздел не найден"},status:501 and return
    end
    render json: section.screencasts.map{|sc| {id:sc.id,no:sc.no,name: sc.name,logo:sc.logo.url(:thumb),video:sc.video.url()}}
  end
end
