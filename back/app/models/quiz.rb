class Quiz < ApplicationRecord
  belongs_to :screencast
  has_many :options
  has_many :imgs
end
