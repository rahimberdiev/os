Rails.application.routes.draw do
  scope '/api', constraints: { format: 'json' } do
    get 'profile/read'

    get 'profile/write'

    post 'sessions/create'
    delete 'sessions/destroy'

    post 'signout/create'

    post 'subjects/list'
    post 'courses/list'

    post 'quizzes/list'
    post 'quizzes/check'
    post 'exams/list'
    post 'exams/check'

    post 'sections/list'
    post 'sections/move'
    post 'sections/current'
    post 'sections/activate'
  end
end
