require 'test_helper'

class ProfileControllerTest < ActionDispatch::IntegrationTest
  test "should get read" do
    get profile_read_url
    assert_response :success
  end

  test "should get write" do
    get profile_write_url
    assert_response :success
  end

end
