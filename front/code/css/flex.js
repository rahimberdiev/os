export const flex = {
  flex_row: {
    display: "flex",
    flexDirection: "row",
  },
  flex_row_reverse: {
    display: "flex",
    flexDirection: "row-reverse",
  },
  flex_col:{
    display:"flex",
    flexDirection:"column",
    justifyContent:"center",
    alignItems: "center",
    height: "100%",
  },
  flex_row_wrap: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
    overflow: 'hidden',
  },
};
