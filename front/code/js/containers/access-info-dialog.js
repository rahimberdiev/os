import PropTypes from 'prop-types';
import React, {Component} from 'react';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import withMobileDialog from '@material-ui/core/withMobileDialog';
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
  cost:{
    color: "red",
  }
});

class AccessInfoDialog extends React.Component {
  componentDidMount() {
  }
  render() {
    const { fullScreen } = this.props;
    const { classes } = this.props;
    return (
      <Dialog
        fullScreen={fullScreen}
        open={this.props.open}
        onClose={()=>this.props.handleClose()}
        aria-labelledby="responsive-dialog-title"
      >
        <DialogTitle id="responsive-dialog-title">{this.props.lang==0?"Абоненттик төлөм":"Абонентская плата"}</DialogTitle>
        <DialogContent>
          <DialogContentText>
            <Typography variant="headline" gutterBottom>
            </Typography>
            <Typography variant="subheading" gutterBottom>
{this.props.lang==0?"2019 жылдын 30-июнуна чейин ЖРТ(ОРТ) курсунун толук версиясын колдонуу үчүн абоненттик төлөм":"Абонентская плата за полный доступ к ресурсам сайта до 30.06.2019 составляет"}
            </Typography>
            <Typography variant="display1" gutterBottom align="center" className={classes.cost}>
              {this.props.lang==0?"500 сомду түзөт. Онлайн курс 2019 жылдын 30-июнуна чейин сизге толук ачык болот.":"500 сом в год. Онлайн курс будет доступен до 30-июня 2019г."}
            </Typography>
            <Typography variant="subheading" gutterBottom>
              {this.props.lang==0?"Төлөмдү «Мобильные сети» терминалдары, мобилдик колдонмолор «Мобильный кошелек» жана башка электрондук капчыктар аркылуу, же болбосо, Кыргыз Инвестициялык Кредиттик Банкы (KICB) аркылуу төлөй аласыздар.Төлөмдөр боюнча бардык суроолор учун WhatsApp байланыш номери 0700854775":"Абонентскую плату вы можете оплатить через терминалы «Мобильные сети» либо через мобильное приложение «Мобильный кошелек» и другие электронные кошелки или через банк KICB. По всем вопросам оплаты пишите на WhatsApp номер 0700854775."}
            </Typography>
            <div style={{display:"flex",justifyContent: "center"}}>
              <img src="/mobilnik-logo.svg" style={{width:180}}/>
            </div>
          </DialogContentText>
        </DialogContent>
      <DialogActions>
        <Button onClick={(event)=>this.props.handleClose()} color="primary">
              {this.props.lang==0?"Жап":"Закрыть"}
        </Button>
      </DialogActions>
    </Dialog>  
    )
  }
}

AccessInfoDialog.propTypes = {
  fullScreen: PropTypes.bool.isRequired,
};

export default withMobileDialog()(withStyles(styles)(AccessInfoDialog));
