import PropTypes from 'prop-types';
import React, {Component} from 'react';
import MaskedInput from 'react-text-mask';
import NumberFormat from 'react-number-format';

const TextMaskPhone = function (props) {
  const { inputRef, ...other } = props;

  return (
    <MaskedInput
      {...other}
      ref={inputRef}
      mask={['0',/\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/]}
      placeholderChar={'\u2000'}
      showMask
    />
  );
}

TextMaskPhone.propTypes = {
  inputRef: PropTypes.func.isRequired,
};

export default TextMaskPhone;
