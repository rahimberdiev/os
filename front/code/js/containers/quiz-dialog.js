import React from 'react';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import withMobileDialog from '@material-ui/core/withMobileDialog';

import MobileStepper from '@material-ui/core/MobileStepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import StepContent from '@material-ui/core/StepContent';
import Paper from '@material-ui/core/Paper';

import SentimentDissatisfied from '@material-ui/icons/SentimentDissatisfied';
import SentimentSatisfied from '@material-ui/icons/SentimentSatisfied';
import SentimentVeryDissatisfied from '@material-ui/icons/SentimentVeryDissatisfied';
import SentimentVerySatisfied from '@material-ui/icons/SentimentVerySatisfied';

import Typography from '@material-ui/core/Typography';
import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';

import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import SwipeableViews from 'react-swipeable-views';
import MathJax from 'react-mathjax-preview'

import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

import {flex} from '../../css/flex'

import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
  icon: {
    margin: theme.spacing.unit,
    fontSize: 64,
  },
  ...flex,
  detailsHeader: {
    padding:0,
  },
  detailsBody: {
    flexDirection:"column",
    padding:0,
  },
  mathjax: {
    display:"inline-flex",
  },
});
class QuizDialog extends React.Component {
  componentDidMount() {
  }
  render() {
    const { fullScreen } = this.props;
    const { classes } = this.props;
    return (
      <div>
        <Dialog
          fullScreen={fullScreen}
          open={this.props.open}
          onClose={()=>this.props.handleClose()}
          aria-labelledby="responsive-dialog-title"
        >
          <DialogTitle id="responsive-dialog-title">{this.props.lang==0 ? `№${(this.props.sc ? this.props.sc.no: '')} сабак боюнча тапшырма` : `Тест по уроку №${(this.props.sc ? this.props.sc.no: '')}`}</DialogTitle>
          <DialogContent>
            <DialogContentText>
            </DialogContentText>

            {!this.props.quizzes.checked&&(
              <div className={classes.root}>
                <SwipeableViews
                  axis={'x'}
                  index={this.props.quizzes.index}
                  onChangeIndex={(index)=>index>this.props.quizzes.index?this.props.moveNext(this.props.quizzes.index):this.props.moveBack(this.props.quizzes.index)}
                  enableMouseEvents
                >
                  {this.props.quizzes.list.map(quiz => (
                    <div key={`quiz_${quiz.id}`}>
                      {quiz.isformula?(<MathJax math={quiz.question} />): (quiz.question && quiz.question.trim().length>0&&(
                        <Typography variant="subheading">
                          {quiz.no}. {quiz.question}
                        </Typography>)
                      )}

                  {quiz.imgs.map(img=>(<img src={img.img} alt={img.title}/>))}

                      <ExpansionPanel expanded={this.props.quizzes.list[this.props.quizzes.index].id===quiz.id && this.props.quizzes.expanded} onChange={()=>this.props.expand(!this.props.quizzes.expanded)}>
                        <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />} className={classes.detailsHeader}>
                          <Typography className={classes.heading}>{(this.props.lang==0 ? "толук маалымат" : "Раскрыть детали вопроса")}</Typography>
                        </ExpansionPanelSummary>
                        <ExpansionPanelDetails className={classes.detailsBody}>
                          {quiz.details.split("\n").map((str,i) =>(
                            <Typography key={`paragraph_${i}`}>
                              {str}
                            </Typography>))
                          }
                        </ExpansionPanelDetails>
                      </ExpansionPanel>
                      <FormControl component="fieldset" className={classes.formControl}>
                        {quiz.options.map((ans,index)=>(
                          <div>
                            <Radio
                              key={`ans_${index}`} value={ans.id.toString()}
                              checked={ans.id.toString() === quiz.answer}
                              value={ans.id.toString()} 
                              onChange={(event)=>{this.props.select(quiz.id,event.target.value)}}
                              name="radio-button-demo"
                              aria-label="A"
                            />
                            {ans.isformula?( <MathJax math={ans.value} className={classes.mathjax}  />): ans.value}
                          </div>
                        ))}
                      </FormControl>
                    </div>
                  ))}
                </SwipeableViews>


                <MobileStepper
                  steps={this.props.quizzes.list.length}
                  position="static"
                  activeStep={this.props.quizzes.index}
                  className={classes.mobileStepper}
                  nextButton={
                    <Button size="small" onClick={(event)=>{this.props.quizzes.index<this.props.quizzes.list.length-1?this.props.moveNext():this.props.check()}}>
                        {this.props.quizzes.index===this.props.quizzes.list.length - 1?(this.props.lang==0?"Текшер":"Завершить"):(this.props.lang==0?"Алдыга":"Далее")}
                        <KeyboardArrowRight />
                      </Button>
                  }
                  backButton={
                    <Button size="small" onClick={()=>this.props.moveBack()} disabled={this.props.quizzes.index === 0}>
                      <KeyboardArrowLeft />
                      {this.props.lang==0?"Артка":"Назад"}
                    </Button>
                  }
                />
              </div>)
            }
            {this.props.quizzes.checked&&(
              <Paper square elevation={0} className={classes.flex_col}>
                {this.props.quizzes.score<50&&<SentimentVeryDissatisfied className={classes.icon} color="secondary"/>||
                this.props.quizzes.score<75&&<SentimentDissatisfied className={classes.icon} color="secondary"/>||
                this.props.quizzes.score<85&&<SentimentSatisfied className={classes.icon} color="primary"/>||
                this.props.quizzes.score<50&&<SentimentVerySatisfied className={classes.icon} color="primary"/>}
                <Typography variant="subheading" gutterBottom align="center" >
              {this.props.lang==0?"Тестин жыйынтыгы":"Ваш результат"}
                  Ваш результат 
                </Typography>
                <Typography variant="display1" gutterBottom align="center" >
                  {this.props.quizzes.score}
                </Typography>
              </Paper>
            )}
          </DialogContent>
          <DialogActions>
            <Button onClick={()=>this.props.handleClose(false)} color="primary">
                      {this.props.lang==0?"Артка кайт":"Закрыть"}
            </Button>
            <Button onClick={()=>this.props.handleClose(true)} color="primary">
                      {this.props.lang==0?"Кийинки сабакка өтүү":"Перейти на следующий урок"}
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}

QuizDialog.propTypes = {
  fullScreen: PropTypes.bool.isRequired,
};

export default withMobileDialog()(withStyles(styles)(QuizDialog));
