import React from 'react';
import videojs from 'video.js'

export default class VideoPlayer extends React.Component {
  componentDidMount() {
    this.player = videojs(this.videoNode, {
      autoplay: true,
      controls: true,
      fluid:true,
    }, (function onPlayerReady() {
      /*      this.player.tech_.hls.xhr.beforeRequest = (function(options) {
        options.uri = options.uri+(options.uri.indexOf('?')>0?'&':'?') +`sc=${this.props.scid}&st=${this.props.st}`;
        return options;
      }).bind(this);*/
    }).bind(this));
  }
  componentDidUpdate(prevProps, prevState){
    if(this.props.paused)
      this.player.pause();
    if(this.props.fill)
      this.player.fill();
  }

  componentWillUnmount() {
    if (this.player) {
      this.player.dispose()
    }
  }

  render() {
    return (
      <div>    
        <div data-vjs-player>
          <video ref={ node => this.videoNode = node } className="video-js">
            {this.props.children}
          </video>
        </div>
      </div>
    )
  }
}
