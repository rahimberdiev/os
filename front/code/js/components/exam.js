import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {orange, blue, grey } from '@material-ui/core/colors';
import { withRouter } from 'react-router-dom'
import { withStyles } from '@material-ui/core/styles';
import withWidth, { isWidthUp, isWidthDown } from '@material-ui/core/withWidth';

import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import Hidden from '@material-ui/core/Hidden';
import MobileStepper from '@material-ui/core/MobileStepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import StepContent from '@material-ui/core/StepContent';

import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';
import SentimentDissatisfied from '@material-ui/icons/SentimentDissatisfied';
import CheckCircle from '@material-ui/icons/CheckCircle';
import Typography from '@material-ui/core/Typography';

import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import SwipeableViews from 'react-swipeable-views';
import Divider from '@material-ui/core/Divider';

import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import MathJax from 'react-mathjax-preview'

import {flex} from '../../css/flex'
import {red,green} from '@material-ui/core/colors';

import {move} from '../actions/sections-api';
import {activate} from '../actions/screencasts-api';
import {load,check} from '../actions/exams-api';
import {select,expand} from '../actions/exams';

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing.unit * 2,
    margin: theme.spacing.unit,
    color: theme.palette.text.secondary,
  },
  button: {
    marginTop: theme.spacing.unit,
    marginRight: theme.spacing.unit,
  },
  icon: {
    margin: theme.spacing.unit,
    fontSize: 64,
  },
  ...flex,
  detailsHeader: {
    padding:0,
  },
  detailsBody: {
    flexDirection:"column",
    padding:0,
  },
  mathjax: {
    display:"inline-flex",
  },
});

class Exam extends Component{
  constructor(props){
    super(props);
  }
  componentWillMount() {
  }
  componentDidMount() {
    this.props.load(this.props.token,this.props.section_id)
  }
  componentWillUnmount() {
  }
  componentWillUpdate () {
  }
  componentDidUpdate(prevProps, prevState){
  }
  check(){
    this.props.check(this.props.token, this.props.section_id,
      this.props.exams.list.map((ex)=>({id:ex.id,ans:ex.answer})))
  }
  move(){
    this.props.move(this.props.token, this.props.course_id)
    this.props.history.push(`/main/${this.props.token}/course/${this.props.subject_id}/${this.props.course_id}`)
  }
  restartSection(){
    this.props.activate(this.props.token,this.props.section_id,-1)
    setTimeout(()=>
    this.props.history.push(`/main/${this.props.token}/course/${this.props.subject_id}/${this.props.course_id}`),200)
  }

  render(){
    const { classes } = this.props;
    return (
      <div className={classes.root}>
        <div>
          {this.props.exams.list.map((exam,index)=>(
            <Paper key={`exam_${exam.id}`} className={classes.paper} elevation={1}>
              {exam.isformula?(<MathJax math={exam.question}/>): (exam.question&&exam.question.trim().length>0&&(
                <Typography variant="subheading">
                  {exam.no}. {exam.question}
                </Typography>)
              )}
              {exam.pics.map(pic=>(<img src={pic.img} alt={pic.title}/>))}
              <ExpansionPanel key={`details_${exam.id}`} expanded={exam.expanded} onChange={()=>this.props.expand(exam.id)}>
                <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />} className={classes.detailsHeader}>
                  <Typography className={classes.heading}>{this.props.session.lang==0?"Толук маалымат":"Раскрыть детали вопроса"}</Typography>
                </ExpansionPanelSummary>
                <ExpansionPanelDetails className={classes.detailsBody}>
                  {exam.details.split("\n").map((str,i) =>(
                    <Typography key={`paragraph_${i}`}>
                      {str}
                    </Typography>))
                  }
                </ExpansionPanelDetails>
              </ExpansionPanel>
              <br/>
              <Divider/>
              <FormControl component="fieldset" className={classes.formControl}>
                {exam.options.map((ans,index)=>(
                  <div>
                    <Radio
                      key={`ans_${index}`} value={ans.id.toString()}
                      checked={ans.id.toString() === exam.answer}
                      value={ans.id.toString()} 
                      onChange={(event)=>{this.props.select(exam.id,event.target.value)}}
                      name="radio-button-demo"
                      aria-label="A"
                    />
                    {ans.isformula?( <MathJax math={ans.value} className={classes.mathjax} />): ans.value}
                  </div>
                ))}
              </FormControl>
            </Paper>))
          }

          <Button variant="outlined" onClick={(event)=>this.check()} className={classes.button}>
            {this.props.session.lang==0?"Сынактын жыйынтыгы":"Завершить экзамен"}
          </Button>
        </div>

        {this.props.exams.checked&&(
          <Paper square elevation={0} className={classes.flex_col}>
            {this.props.exams.passed?<CheckCircle className={classes.icon} color="primary"/> : <SentimentDissatisfied className={classes.icon} color="secondary"/>}
            <Typography variant="headline" gutterBottom align="center" >
              {this.props.session.lang==0?"Урматтуу":"Уважаемый"} {this.props.session.fullname}!
            </Typography>
            <Typography variant="subheading" gutterBottom align="center" >
              {this.props.exams.passed?(this.props.session.lang==0?"Сынак ийгиликтүү өтүлдү":"Тест успешно пройден!"):(this.props.session.lang==0?"Сынак өтүлгөн жок!":"Тест не пройден!")}
            </Typography>
            <Typography variant="subheading" gutterBottom align="center" >
              {this.props.session.lang==0?"Сынактын жыйынтыгы":"Ваш результат"}
            </Typography>
            <Typography variant="display1" gutterBottom align="center" >
              {this.props.exams.score}% 
            </Typography>
            <Button variant="outlined" onClick={(event)=>this.restartSection()} className={classes.button}>
              {this.props.session.lang==0?"Бөлүмдү кайрадан кайталоо":"Начать заново"}
            </Button>
            <Button variant="outlined" onClick={(event)=>this.move()} className={classes.button} disabled={!this.props.exams.passed}>
              {this.props.session.lang==0?"Кийинки бөлүмгө өтүү":"Перейти в следующую секцию"}
            </Button>
            <Typography variant="overline" gutterBottom style={{color:"red",margin:15}} align="center">
              {this.props.session.lang==0?"Эскертүү: Эгерде кийинки бөлүмгө өтсөңүз, бул бөлүм сиз үчүн жеткиликсиз болот!":"Внимание: При переходе на следующий раздел, настоящий раздел закроется и не будет доступен!"}
            </Typography>
          </Paper>
        )}

      </div>
    );
  }
}

function mapStateToProps(state,ownProps){
  return {
    subject_id: ownProps.match.params.subject_id,
    course_id: ownProps.match.params.course_id,
    section_id: ownProps.match.params.section_id,
    token: ownProps.match.params.token,
    session: state.session,
    exams: state.exams,
    section: state.sections.list.find(s=>s.id===state.sections.active_id),
  };
}
function matchDispatchToProps(dispatch){
  return bindActionCreators({
    load,
    check,
    move,
    select,
    activate,
    expand,
  },dispatch);
}

export default withRouter(connect(mapStateToProps,matchDispatchToProps)(withWidth()((withStyles(styles)(Exam)))));
