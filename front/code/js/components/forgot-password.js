import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {orange, blue, grey, blueGrey } from '@material-ui/core/colors';
import { withRouter } from 'react-router-dom'
import { withStyles } from '@material-ui/core/styles';

import PropTypes from 'prop-types';
import AccountCircle from '@material-ui/icons/AccountCircle';
import Phone from '@material-ui/icons/Phone';
import VpnKey from '@material-ui/icons/VpnKey';

import InputAdornment from '@material-ui/core/InputAdornment';

import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';

import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';

import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import MaskedInput from 'react-text-mask';
import NumberFormat from 'react-number-format';
import {changeUsername,changePassword,changePasswordConf,changePin,changeLang} from '../actions/forgot-password'
import {change,approve} from '../actions/forgot-password-api';
import {clear} from '../actions/forgot-password';
import {loggedIn} from '../actions/session';
import {flex} from '../../css/flex';

const styles = theme => ({
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 240,
  },
  ...flex,
  icon: {
    position: 'absolute',
    left: 330,
    top: 15,
  },
});

class ForgotPassword extends Component{
  componentWillUpdate () {
  }
  componentWillReceiveProps (newProps){
    const {session} = newProps;
    session.token&&this.props.history.push(`/main/${session.token}`)
  }
  render(){
    const { classes } = this.props;
    return (
      <div className={classes.flex_col} style={{}}>
        {(this.props.forgot_password.step==0)&&(
          <div className={classes.flex_col} style={{}}>
            <IconButton className={classes.icon} onClick={(event)=>this.props.history.push('/')}>
              <CloseIcon />
            </IconButton>
            <img src="/logo.jpg" style={{width:280}}></img>
            <br/>
            <Typography style={{color:"red"}} align="center">
              {this.props.forgot_password.message}
            </Typography>
            <br/>
            <FormControl component="fieldset" style={{width:240}}>
              <FormLabel component="legend"><small>Окуу тили/Язык обучения</small></FormLabel>
              <RadioGroup
                aria-label="Gender"
                name="gender1"
                className={classes.group}
                value={this.props.forgot_password.lang}
                onChange={event=>this.props.changeLang(parseInt(event.target.value))}
                style={{flexDirection:"row"}}
              >
                <FormControlLabel value={0} control={<Radio />} label="Кыр" />
                <FormControlLabel value={1} control={<Radio />} label="Рус" />
              </RadioGroup>
            </FormControl>
            <TextField
              id="id_username"
              label={this.props.forgot_password.lang==0?"Телефон номурунуз":"Номер телефона"}
              autoFocus
              className={classes.textField}
              value={this.props.forgot_password.username}
              onChange={event=>this.props.changeUsername(event.target.value)}
              margin="normal"
              error={this.props.forgot_password.username_error}
              helperText={this.props.forgot_password.username_error}
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start">
                    <Phone />
                  </InputAdornment>
                ),
              }}
            />
            <br/>
            <Button variant="contained" color="primary" size="large" onClick={()=>{this.props.change(this.props.forgot_password.username,this.props.forgot_password.lang)}}>
              {this.props.forgot_password.lang==0?"SMS кодду жөнөт":"Получить одноразовый код"}
            </Button>
            <br/>
          </div>)
        }
        {(this.props.forgot_password.step==1)&&(
          <div className={classes.flex_col} style={{}}>
            <IconButton className={classes.icon} onClick={(event)=>this.props.history.push('/')}>
              <CloseIcon />
            </IconButton>
            <img src="/logo.jpg" style={{width:280}}></img>
            <br/>
            <Typography style={{color:"red"}} align="center">
              {this.props.forgot_password.message}
            </Typography>
            <br/>
            <TextField
              id="id_pin"
              label={this.props.forgot_password.lang==0?"Жашыруун SMS код":"Код подтверждения"}
              className={classes.textField}
              value={this.props.forgot_password.pin}
              onChange={event=>this.props.changePin(event.target.value)}
              margin="normal"
              error={this.props.forgot_password.pin_error}
              helperText={this.props.forgot_password.pin_rror}
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start">
                    <VpnKey />
                  </InputAdornment>
                ),
              }}
            />
            <TextField
              id="id_password"
              label={this.props.forgot_password.lang==0?"Жаны пароль":"Новый пароль"}
              className={classes.textField}
              value={this.props.forgot_password.password}
              onChange={event=>this.props.changePassword(event.target.value)}
              margin="normal"
              error={this.props.forgot_password.password_error}
              helperText={this.props.forgot_password.password_error}
              type="password"
              autoComplete="current-password"
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start">
                    <VpnKey />
                  </InputAdornment>
                ),
              }}
            />
            <TextField
              id="id_password_conf"
              label={this.props.forgot_password.lang==0?"Парольду кайталоо":"Подтверждение пароля"}
              className={classes.textField}
              value={this.props.forgot_password.password_conf}
              onChange={event=>this.props.changePasswordConf(event.target.value)}
              margin="normal"
              error={this.props.forgot_password.password_conf_error}
              helperText={this.props.forgot_password.password_conf_error}
              type="password"
              autoComplete="current-password"
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start">
                    <VpnKey />
                  </InputAdornment>
                ),
              }}
            />
            <Button variant="contained" color="primary" size="large" onClick={()=>{this.props.approve(this.props.forgot_password.username,this.props.forgot_password.password,this.props.forgot_password.password_conf,this.props.forgot_password.pin,this.props.forgot_password.lang)}}>
              {this.props.forgot_password.lang?"Парольду алмаштыр":"Обновить"} 
            </Button>
            <br/>
          </div>
        )}
        {(this.props.forgot_password.step==2)&&(
          <div className={classes.flex_col} style={{}}>
            <IconButton className={classes.icon} onClick={(event)=>this.props.history.push('/')}>
              <CloseIcon />
            </IconButton>
            <img src="/logo.jpg" style={{width:280}}></img>
            <br/>
            <Typography style={{color:"green"}} align="center" variant="headline">
              {this.props.forgot_password.lang==0?"Пароль ийгиликтүү алмашылды":"Пароль успешно обновлен"}
            </Typography>
            <br/>
            <br/>
            <Button variant="contained" color="primary" size="large" onClick={()=>{this.props.loggedIn({
              token:this.props.forgot_password.token,
              token_created_at:this.props.forgot_password.token_created_at,
              fullname:this.props.forgot_password.fullname,
              username:this.props.forgot_password.username,
              payments:this.props.forgot_password.payments,
              full_access:this.props.forgot_password.full_access});this.props.clear()}}>
              {this.props.forgot_password.lang==0?"Улантуу":"Продолжить"}
            </Button>
            <br/>
        </div>
        )}
        <br/>
        <a href="https://wa.me/996700854775" className={classes.flex_row} style={{alignItems:"center",textDecoration:"none"}}>
          <img src="/whatsapp.png" style={{width:48,margin:10}}/>
          <Typography variant="subheading" gutterBottom>
            +996700854775
          </Typography>
        </a>
      </div>
    );
  }
}

function mapStateToProps(state){
  return {
    forgot_password: state.forgot_password,
    session: state.session,
  };
}
function matchDispatchToProps(dispatch){
  return bindActionCreators({
    changeUsername,
    changePassword,
    changePasswordConf,
    changePin,
    changeLang,
    change,
    approve,
    loggedIn,
    clear,
  },dispatch);
}

export default withRouter(connect(mapStateToProps,matchDispatchToProps)(withStyles(styles)(ForgotPassword)));
