import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {orange, blue, grey, blueGrey } from '@material-ui/core/colors';
import { withRouter } from 'react-router-dom'
import { withStyles } from '@material-ui/core/styles';

import PropTypes from 'prop-types';
import Phone from '@material-ui/icons/Phone';
import VpnKey from '@material-ui/icons/VpnKey';
import InputAdornment from '@material-ui/core/InputAdornment';

import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Hidden from '@material-ui/core/Hidden';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';

import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import Checkbox from '@material-ui/core/Checkbox';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';

import {changeUsername,changePassword,changeLang} from '../actions/session'
import {login} from '../actions/session-api';
import {flex} from '../../css/flex';

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 240,
  },
  flex_row: {
    display: "flex",
    flexDirection: "row",
  },
  flex_col:{
    display:"flex",
    flexDirection:"column",
    justifyContent:"center",
    alignItems: "center",
    height: "100%",
  },
  imgBox:{
    width: "90%",
    margin: "auto",
    display: "block",
    borderRadius: 20,
    cursor:"pointer",
  },
});

class SignIn extends Component{
  componentWillUpdate () {
  }
  componentWillReceiveProps (newProps){
    const {session} = newProps;
    session.token&&this.props.history.push(`/main/${session.token}`)
  }

  render(){
    const { classes } = this.props;
    return (
      <div className={classes.flex_col} style={{minHeight:500}}>
        <img src="logo.jpg" style={{width:280}}></img>
        <br/>
        <Typography style={{color:"red"}}>
          {this.props.session.message}
        </Typography>
        <br/>
        <FormControl component="fieldset" style={{width:240}}>
          <FormLabel component="legend"><small>Окуу тили/Язык обучения</small></FormLabel>
          <RadioGroup
            aria-label="Gender"
            name="gender1"
            className={classes.group}
            value={this.props.session.lang}
            onChange={event=>this.props.changeLang(parseInt(event.target.value))}
            style={{flexDirection:"row"}}
          >
            <FormControlLabel value={0} control={<Radio />} label="Кыр." />
            <FormControlLabel value={1} control={<Radio />} label="Рус." />
          </RadioGroup>
        </FormControl>
        <TextField
          id="id_username"
          label={this.props.session.lang==0?"Телефон номурунуз":"Номер телефона"}
          autoFocus
          className={classes.textField}
          value={this.props.session.username}
          onChange={event=>this.props.changeUsername(event.target.value)}
          margin="normal"
          error={this.props.session.username_error!=''}
          helperText={this.props.session.username_error}
          InputProps={{
            startAdornment: (
              <InputAdornment position="start">
                <Phone />
              </InputAdornment>
            ),
          }}
        />
        <TextField
          id="id_password"
          label={this.props.session.lang==0?"Пароль":"Пароль"}
          className={classes.textField}
          value={this.props.session.password}
          onChange={event=>this.props.changePassword(event.target.value)}
          margin="normal"
          error={this.props.session.password_error!=''}
          helperText={this.props.session.password_error}
          onKeyPress={event => event.key === 'Enter'&&this.props.login(this.props.session.username,this.props.session.password,this.props.session.lang)}

          type="password"
          autoComplete="current-password"
          InputProps={{
            startAdornment: (
              <InputAdornment position="start">
                <VpnKey/>
              </InputAdornment>
            ),
          }}
        />
        <br/>
        <Button variant="contained" color="primary" size="large" onClick={()=>{this.props.login(this.props.session.username,this.props.session.password,this.props.session.lang)}}>
          {this.props.session.lang==0?"Кирүү":"Войти"}
        </Button>
        <br/>
        <Button size="small" onClick={()=>{this.props.history.push('/init/forgot')}}>
          {this.props.session.lang==0?"Паролду унуттум":"Забыли пароль?"} 
        </Button>
        <Button variant="outlined" size="large" color="secondary" onClick={(event)=>{this.props.history.push(`/init/signout`)}}>
          {this.props.session.lang==0?"Системага катталуу":"Зарегистрироваться"}
        </Button>
      </div>
    );
  }
}

function mapStateToProps(state){
  return {
    session: state.session,
    paths: state.paths,
  };
}
function matchDispatchToProps(dispatch){
  return bindActionCreators({
    changeUsername:changeUsername,
    changePassword:changePassword,
    changeLang:changeLang,
    login:login,
    formula:`$$\[\frac{-b\pm\sqrt{b^2-4ac}}{2a}+\frac{12}{2}+\frac{\text{d}x}{\text{d}y}\]$$`,
  },dispatch);
}

export default withRouter(connect(mapStateToProps,matchDispatchToProps)(withStyles(styles)(SignIn)));
