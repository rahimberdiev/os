import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {orange, blue, grey,green } from '@material-ui/core/colors';
import { withRouter } from 'react-router-dom'
import { withStyles } from '@material-ui/core/styles';
import { Route, Link, Switch } from "react-router-dom";

import {load} from '../actions/subjects-api';

import Subject from './subject';
import Probs from './probs';
import ProbTest from './prob-test';
import Subjects from './subjects';
import Course from './course';
import Screencast from './screencast';
import Exam from './exam';

import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';
import SwipeableDrawer from '@material-ui/core/SwipeableDrawer';
import Drawer from '@material-ui/core/Drawer';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import List from '@material-ui/core/List';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import Snackbar from '@material-ui/core/Snackbar';
import CloseIcon from '@material-ui/icons/Close';

import Hidden from '@material-ui/core/Hidden';
import Divider from '@material-ui/core/Divider';
import Paper from '@material-ui/core/Paper';
import MenuIcon from '@material-ui/icons/Menu';
import AccountCircle from '@material-ui/icons/AccountCircle';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Avatar from '@material-ui/core/Avatar';
import InboxIcon from '@material-ui/icons/Inbox';
import FaceIcon from '@material-ui/icons/Face';
import Button from '@material-ui/core/Button';
import {logout, refreshSession} from '../actions/session-api';

import {pOpen, pClose,mOpen,mClose} from '../actions/menu'
import {closeSnack} from '../actions/snack'
import {close as close_access_info,open as open_access_info} from '../actions/access-info'

import AccessInfoDialog from '../containers/access-info-dialog'

const drawerWidth = 340;

const styles = theme => ({
  root: {
    flexGrow: 1,
    height: '100%',
    zIndex: 1,
    overflow: 'hidden',
    position: 'relative',
    display: 'flex',
    width: '100%',
  },
  flex: {
    flexGrow: 1,
  },
  appBar: {
    position: 'absolute',
    marginLeft: drawerWidth,
    [theme.breakpoints.up('md')]: {
      width: `calc(100% - ${drawerWidth}px)`,
    },
  },
  navIconHide: {
    [theme.breakpoints.up('md')]: {
      display: 'none',
    },
  },
  toolbar: theme.mixins.toolbar,
  drawerPaper: {
    width: drawerWidth,
    [theme.breakpoints.up('md')]: {
      position: 'relative',
    },
  },
  content: {
    flexGrow: 1,
    overflowX:"scroll",
    backgroundColor: theme.palette.background.default,
    [theme.breakpoints.down('md')]: {
      paddingTop: theme.spacing.unit 
    },
    [theme.breakpoints.up('lg')]: {
      padding: theme.spacing.unit * 3
    },
  },
  greenAvatar: {
    margin: 10,
    color: '#fff',
    backgroundColor: green[500],
  },
  fullname: {
    position: 'absolute',
    left: 15,
    top: 15,
  },
  full_access: {
    color: "white",
  }
});
class Main extends Component{
  componentWillMount() {
    if(!this.props.token||this.props.token==='')
      this.props.history.push('/')
    if(this.props.session.token==="")
      this.props.refreshSession(this.props.token)

    this.props.load(this.props.token)
  };
  componentDidUpdate(prevProps, prevState){
    if(this.props.session.access_denied)
      this.props.history.push('/')
  }

  gotoProfile(){
    this.props.pClose();
  }
  logout(){
    this.props.pClose();
    this.props.logout(this.props.token);
    this.props.history.push('/')
  }
  openSubject(subject){
    this.props.mClose();
    this.props.history.push(`${this.props.match.url}/subject/${subject.id}`)
  }
  openTesting(prob){
    this.props.mClose();
    this.props.history.push(`${this.props.match.url}/prob/${prob.id}`)
  }
  handleClose(reason) {
    if (reason === 'clickaway') {
      return;
    }
    this.props.closeSnack();
  };

  render() {
    const { classes, theme } = this.props;

    const drawer = (
      <div>
        <div className={classes.toolbar} >
        <ListItem>
          <Avatar className={classes.greenAvatar}>
            <FaceIcon />
          </Avatar>
          <ListItemText primary={this.props.session.fullname} secondary={<div>{this.props.session.phone}<br/> {this.props.session.lang == 0 ?"Төлөнгөн сумма:" : "Сумма оплаты:"} {this.props.session.payments} сом</div>} />
        </ListItem>
      </div>
        {this.props.subjects.list.map(subject=>(
          <ListItem key={`subj_${subject.id}`} button onClick={(event)=>this.openSubject(subject)}>
            <ListItemIcon>
              <InboxIcon />
            </ListItemIcon>
            <ListItemText primary={subject.name}/>
          </ListItem>))
        }
            <Divider />
            {(!this.props.session.full_access)&&(
              <ListItem key={`probs`} button onClick={(event)=>{this.props.history.push(`${this.props.match.url}/probs`)}}>
                <ListItemIcon>
                  <InboxIcon />
                </ListItemIcon>
                <ListItemText primary={this.props.session.lang == 0 ? "Сынамык тест":"Пробный тест"} secondary={"(beta версия)"}/>
              </ListItem>)
            }
          </div>
    );
  

    return (
      <div className={classes.root}>
        <AppBar className={classes.appBar}>
          <Toolbar>
            <IconButton
              color="inherit"
              aria-label="Open drawer"
              onClick={this.props.mOpen}
              className={classes.navIconHide}
            >
              <MenuIcon />
            </IconButton>
            <img src="/apgrade_white.png" style={{width:120}}></img>
            <span className={classes.flex}>
              {(!this.props.session.full_access)&&(
                <Button color="secondary" onClick={(event)=>this.props.open_access_info()} className={classes.full_access}>
                  {this.props.session.lang ==0 ? "чектелген":"огранич.доступ"}
                </Button>)
              }
              {this.props.session.full_access&&(
                <Typography variant="caption" gutterBottom className={classes.full_access}>
                  {this.props.session.lang ==0 ? "толук версия":"полный доступ"}
                </Typography>)
              }
            </span>
            {this.props.token && (
              <div>
                <IconButton
                  aria-owns={this.props.menu.p_opened ? 'menu-appbar' : null}
                  aria-haspopup="true"
                  onClick={event => this.props.pOpen(event.currentTarget)}
                  color="inherit"
                >
                  <AccountCircle />
                </IconButton>
                <Menu
                  id="menu-appbar"
                  anchorEl={this.props.menu.p_anchor}
                  anchorOrigin={{
                    vertical: 'top',
                    horizontal: 'right',
                  }}
                  transformOrigin={{
                    vertical: 'top',
                    horizontal: 'right',
                  }}
                  open={this.props.menu.p_opened}
                  onClose={this.props.pClose}
                >
                  <MenuItem onClick={(event)=>this.logout()}>{this.props.session.lang==0?"Чыгуу":"Выход"}</MenuItem>
                </Menu>
              </div>
            )}
          </Toolbar>
        </AppBar>
        <Hidden mdUp>
          <SwipeableDrawer
            variant="temporary"
            anchor='left'
            open={this.props.menu.m_opened}
            onClose={this.props.mClose}
            onOpen={this.props.mOpen}
            classes={{
              paper: classes.drawerPaper,
            }}
            ModalProps={{
              keepMounted: true, // Better open performance on mobile.
            }}
          >
            {drawer}
          </SwipeableDrawer>
        </Hidden>
        <Hidden smDown implementation="css">
          <Drawer
            variant="permanent"
            open
            classes={{
              paper: classes.drawerPaper,
            }}
          >
            {drawer}
          </Drawer>
        </Hidden>        
        <main className={classes.content}>
          <div className={classes.toolbar} />
          <Switch>
            <Route exact path={`/main/:token/`} component={Subjects}/>
            <Route exact path={`/main/:token/probs/`} component={Probs}/>
            <Route exact path={`/main/:token/probs/test/:id`} component={ProbTest}/>
            <Route exact path={`/main/:token/subject/:id`} component={Subject}/>
            <Route exact path={`/main/:token/course/:subject_id/:id`} component={Course}/>
            <Route exact path={`/main/:token/screencast/:subject_id/:course_id/:section_id/:id`} component={Screencast}/>
            <Route exact path={`/main/:token/exam/:subject_id/:course_id/:section_id`} component={Exam}/>
          </Switch>
          <Snackbar
            anchorOrigin={{
              vertical: 'bottom',
              horizontal: 'center',
            }}
            open={this.props.snack.open}
            autoHideDuration={3000}
            onClose={(event,reason)=>this.handleClose(reason)}
            ContentProps={{
              'aria-describedby': 'message-id',
            }}
            message={<span id="message-id">{this.props.snack.message}</span>}
            action={[
              <IconButton
                key="close"
                aria-label="Close"
                color="inherit"
                className={classes.close}
                onClick={(event,reason)=>this.handleClose(reason)}
              >
                <CloseIcon />
              </IconButton>,
            ]}
          />
        </main>
        <AccessInfoDialog open={this.props.access_info.open} handleClose={this.props.close_access_info} fullname={this.props.session.fullname} lang={this.props.session.lang}/>
      </div>
    );
  }
}
Main.propTypes = {
    classes: PropTypes.object.isRequired,
    theme: PropTypes.object.isRequired,
};

function mapStateToProps(state,ownProps){
  return {
    token: ownProps.match.params.token,
    session: state.session,
    subjects: state.subjects,
    menu: state.menu,
    snack: state.snack,
    access_info: state.access_info,
  };
}
function matchDispatchToProps(dispatch){
  return bindActionCreators({
    load,
    pOpen, mOpen, pClose, mClose, 
    logout,
    refreshSession,
    closeSnack:closeSnack,
    close_access_info: close_access_info,
    open_access_info: open_access_info,
  },dispatch);
}

export default withRouter(connect(mapStateToProps,matchDispatchToProps)(withStyles(styles,{withTheme: true})(Main)));
