import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {orange, blue, grey } from '@material-ui/core/colors';
import { withRouter } from 'react-router-dom'
import { withStyles } from '@material-ui/core/styles';

import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import Hidden from '@material-ui/core/Hidden';

import Typography from '@material-ui/core/Typography';
import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';
import MobileStepper from '@material-ui/core/MobileStepper';

import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';

import {nextSC, prevSC} from '../actions/screencasts';

import {load as loadQuiz,check} from '../actions/quizzes-api';
import {load, activate} from '../actions/screencasts-api';
import {moveNext,moveBack,select,open as openQuizDialog,close as closeQuizDialog,expand} from '../actions/quizzes';
import videojs from 'video.js'

import VideoPlayer from '../containers/video-player'

import QuizDialog from '../containers/quiz-dialog'

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing.unit * 2,
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
  button: {
    marginTop: theme.spacing.unit,
    marginRight: theme.spacing.unit,
  },
});

class Screencast extends Component{
  constructor(props){
    super(props);
  }
  componentWillMount() {
    this.props.load(this.props.token,this.props.section_id)
    this.props.activate(this.props.token,this.props.section_id,this.props.id)
    this.props.loadQuiz(this.props.token,this.props.id)

  }
  componentDidMount() {
  }
  componentWillUnmount() {
  }
  componentWillUpdate () {
  }
  componentDidUpdate(prevProps, prevState){
  }
  nextSC(index){
    if(index<this.props.screencasts.list.length-1){
      var sc_id = this.props.screencasts.list[index+1].id
      this.props.activate(this.props.token, this.props.section_id, sc_id)
      this.props.loadQuiz(this.props.token, sc_id)
      this.props.nextSC(index);
    }else
      this.props.history.push(`/main/${this.props.token}/exam/${this.props.subject_id}/${this.props.course_id}/${this.props.section_id}`)
  }
  prevSC(index){
    if(index>0){
      var sc_id = this.props.screencasts.list[index-1].id
      this.props.activate(this.props.token, this.props.section_id, sc_id)
      this.props.loadQuiz(this.props.token, sc_id)
      this.props.prevSC(index);
    }else
      this.props.history.push(`/main/${this.props.token}/course/${this.props.subject_id}/${this.props.course_id}`)
  }
  check(){
    var sc_id = this.props.screencasts.list[this.activeIndex()].id
    this.props.check(this.props.token, sc_id,
      this.props.quizzes.list.map((quiz)=>({id:quiz.id,ans:quiz.answer})))
  }
  closeQuizDialog(next_sc){
    this.props.closeQuizDialog()
    if(next_sc)
      this.nextSC(this.activeIndex())
  }
  activeIndex(){
    return this.props.screencasts.active_sc_index!=-1 ?
      this.props.screencasts.active_sc_index : 
      this.props.screencasts.list.findIndex((el,i)=>el.id===this.props.id)
  }
  render(){
    const { classes } = this.props;
    return (
      <div className={classes.root}>
        <Button size="small" color="primary" onClick={(event)=>this.props.history.push(`/main/${this.props.token}/course/${this.props.subject_id}/${this.props.course_id}`)}>
          <KeyboardArrowLeft />
          {this.props.screencasts.course.name}
        </Button>
        <div>
          {this.props.screencasts.list.map((sc,index) => (index===this.activeIndex())&&(
            <div key={`vp_${sc.id}`} data-vjs-player>
              <VideoPlayer paused={this.props.quizzes.open} st={this.props.token} scid={sc.id}>
                <source src={sc.video+`&sc=${sc.id}&st=${this.props.token}`} type='application/x-mpegURL'/>
              </VideoPlayer>
            </div>))
          }
        </div>
        <MobileStepper
          variant="dots"
          steps={this.props.screencasts.list.length}
          position="static"
          activeStep={this.activeIndex()}
          className={classes.root}
          nextButton={
            <Button variant="outlined" size="small" color="primary" onClick={(event)=>this.props.quizzes.list.length>0?this.props.openQuizDialog():this.nextSC(this.activeIndex())}>
              {this.activeIndex()===this.props.screencasts.list.length-1?(this.props.session.lang==0?"Сынак тапшыруу":"Пройти экзамен"):(this.props.session.lang==0?"Алдыга":"След. урок")}
              <KeyboardArrowRight />
            </Button>
          }
          backButton={
            <Button variant="outlined" size="small" color="primary" onClick={(event)=>this.prevSC(this.activeIndex())}>
              <KeyboardArrowLeft />
              {this.activeIndex()===0?(this.props.session.lang==0?"Артка":"Закрыть"):(this.props.session.lang==0?"Артка":"Пред. урок")}
            </Button>
          }
        /> 
        
        <QuizDialog handleClose={(next_sc)=>this.closeQuizDialog(next_sc)}
          lang = {this.props.session.lang}
          open={this.props.quizzes.open} 
          sc={this.props.screencasts.list[this.activeIndex()]}
          quizzes={this.props.quizzes}
          moveNext={this.props.moveNext}
          moveBack={this.props.moveBack}
          expand={this.props.expand}
          select={this.props.select}
          check={()=>this.check()}
        >
        </QuizDialog>

      </div>
    );
  }
}

function mapStateToProps(state,ownProps){
  return {
    subject_id: parseInt(ownProps.match.params.subject_id),
    course_id: parseInt(ownProps.match.params.course_id),
    section_id: parseInt(ownProps.match.params.section_id),
    id: parseInt(ownProps.match.params.id),
    token: ownProps.match.params.token,

    session: state.session,
    screencasts: state.screencasts,
    quizzes: state.quizzes,
  };
}
function matchDispatchToProps(dispatch){
  return bindActionCreators({
    loadQuiz,check,
    load,
    activate,
    nextSC,
    prevSC,
    moveNext,
    moveBack,
    expand,
    select,
    openQuizDialog,
    closeQuizDialog,
  },dispatch);
}

export default withRouter(connect(mapStateToProps,matchDispatchToProps)(withStyles(styles)(Screencast)));
