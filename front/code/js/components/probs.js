import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {orange, blue, grey } from '@material-ui/core/colors';
import { withRouter } from 'react-router-dom'
import { withStyles } from '@material-ui/core/styles';

import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import Hidden from '@material-ui/core/Hidden';

import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelActions from '@material-ui/core/ExpansionPanelActions';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Divider from '@material-ui/core/Divider';

import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import PlayCircleFilledIcon from '@material-ui/icons/PlayCircleFilled';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';

import {load}  from '../actions/probs-api';

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  list:{
    width:"100%"
  }
});

class Probs extends Component{
  constructor(props){
    super(props);
  }
  componentWillMount() {
    this.props.load(this.props.token,this.props.id)
  }

  componentDidMount() {
  }
  componentWillUnmount() {
  }
  componentWillUpdate () {
  }
  componentDidUpdate(prevProps, prevState){
  }
  render(){
    const { classes } = this.props;
    return (
      <div className={classes.root}>
        <List component="nav" className={classes.list}>
          {this.props.probs.list.map((prob,index)=>(
            <ListItem key={`prob_${prob.id}`} button onClick={(event)=>this.props.history.push(`${this.props.match.url}/test/${prob.id}`)}>
              <ListItemIcon>
                <PlayCircleFilledIcon />
              </ListItemIcon>
              <ListItemText primary={prob.name} secondary={prob.description} />
            </ListItem>))
          }
        </List>
      </div>
    );
  }
}

function mapStateToProps(state,ownProps){
  return {
    session: state.session,
    token: ownProps.match.params.token,
    probs: state.probs,
  };
}
function matchDispatchToProps(dispatch){
  return bindActionCreators({
    load,
  },dispatch);
}

export default withRouter(connect(mapStateToProps,matchDispatchToProps)(withStyles(styles)(Probs)));
