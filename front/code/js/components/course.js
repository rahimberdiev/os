import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {orange, blue, grey } from '@material-ui/core/colors';
import { withRouter } from 'react-router-dom'
import { withStyles } from '@material-ui/core/styles';

import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import Hidden from '@material-ui/core/Hidden';

import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelActions from '@material-ui/core/ExpansionPanelActions';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Divider from '@material-ui/core/Divider';

import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import PlayCircleFilledIcon from '@material-ui/icons/PlayCircleFilled';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';

import {select} from '../actions/sections';
import {load}  from '../actions/sections-api';
import {openSnack } from '../actions/snack';

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    flexBasis: '33.33%',
    flexShrink: 0,
  },
  heading_passed: {
    fontSize: theme.typography.pxToRem(15),
    flexBasis: '33.33%',
    flexShrink: 0,
    color:"green",
  },

  score: {
    fontSize: theme.typography.pxToRem(15),
    color: "green",
  },
  button: {
    marginTop: theme.spacing.unit,
    marginRight: theme.spacing.unit,
  },
  play_icon: {
    color:"red",
  },
  pass_icon: {
    color:"green",
  },
  [theme.breakpoints.down('sm')]: {
    exp_panel:{
      padding:"0 4px 0 4px" 
    },
    list:{
      width:"100%"
    }
  },
  [theme.breakpoints.up('md')]: {
    exp_panel:{
      padding:"0 4px 0 4px" 
    },
    list:{
      width:"100%"
    }
  },
});

class Course extends Component{
  constructor(props){
    super(props);
  }
  componentWillMount() {
    this.props.load(this.props.token,this.props.id)
  }

  componentDidMount() {
  }
  componentWillUnmount() {
  }
  componentWillUpdate () {
  }
  componentDidUpdate(prevProps, prevState){
  }
  select(section,expended){
    if(expended){
      this.props.select(section.id)
    }else
      this.props.select(-1)
  }
  activate(section, sc){
    if(this.props.sections.active_id != section.id){
      this.props.openSnack(this.props.session.lang==0?"Учурдагы бөлүмдүн кана сабактырун көрүүгө мүмкүн. Кийинки бөлүмгө өтүү үчүн сынак тапшырыныз":"Вы можете просматривать уроки только текущего раздела. Чтобы перейти на следующий раздел пройдите экзамен")
      return
    }
    this.props.history.push(`/main/${this.props.token}/screencast/${this.props.subject_id}/${this.props.id}/${this.props.sections.active_id}/${sc.id}`)
  }
  render(){
    const { classes } = this.props;
    return (
      <div className={classes.root}>
        {this.props.sections.list.map((section,index)=>(
          <ExpansionPanel key={`section_${section.id}`} expanded={section.id===this.props.sections.selected_id} onChange={(event,expended)=>this.select(section,expended)}>
            <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />  }>
              {section.score && <CheckCircleIcon className={classes.pass_icon} />}
              <Typography className={section.score ? classes.heading_passed: classes.heading}>{section.name}</Typography>
            </ExpansionPanelSummary>
            <ExpansionPanelDetails className={classes.exp_panel}>
              <List component="nav" className={classes.list}>
                <ListItem key={`sc_info_pass_score`}>
                  <ListItemText primary={section.score ? (this.props.session.lang==0?`Секция өтүлгөн, Сынактын жыйынтыгы:  ${section.score.score}%`: `Секция пройдена, Ваш результат:  ${section.score.score}%`) : (this.props.session.lang==0 ? `өтүүчү балл:  ${section.pass_score}%` : `Проходной балл:  ${section.pass_score}%`)} secondary={''} />
                </ListItem>
                {section.scs.map((sc,index)=>(
                  <ListItem key={`sc_${sc.id}`} button onClick={(event)=>this.activate(section,sc)} disabled={section.id!==this.props.sections.active_id}>
                    <ListItemIcon className={sc.id===this.props.sections.active_sc_id?classes.play_icon:null}>
                      <PlayCircleFilledIcon />
                    </ListItemIcon>
                    <ListItemText primary={sc.name} secondary={sc.description} />
                  </ListItem>))
                }
              </List>
            </ExpansionPanelDetails>
            <Divider />
            <ExpansionPanelActions>
              <Button size="small" color="primary" onClick={(event)=>this.props.history.push(`/main/${this.props.token}/exam/${this.props.subject_id}/${this.props.id}/${section.id}`)} disabled={section.id!==this.props.sections.active_id}>
                {this.props.session.lang==0?"Сынак тапшырып кийинки бөлүмгө өтүңүз":"Пройдите экзамен и перейдите в следующий раздел"}
              </Button>
            </ExpansionPanelActions>
          </ExpansionPanel> ))
        }
      </div>
    );
  }
}

function mapStateToProps(state,ownProps){
  return {
    id: ownProps.match.params.id,
    subject_id: ownProps.match.params.subject_id,
    session: state.session,
    token: ownProps.match.params.token,
    sections: state.sections,
    screencasts: state.screencasts,
    course_progress: state.course_progress,
  };
}
function matchDispatchToProps(dispatch){
  return bindActionCreators({
    load,
    select,
    openSnack,
  },dispatch);
}

export default withRouter(connect(mapStateToProps,matchDispatchToProps)(withStyles(styles)(Course)));
