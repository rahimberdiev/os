import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {orange, blue, grey, blueGrey } from '@material-ui/core/colors';
import { withRouter } from 'react-router-dom'
import { withStyles } from '@material-ui/core/styles';

import PropTypes from 'prop-types';
import VideoPlayer from '../containers/video-player'
import {flex} from '../../css/flex';

const styles = theme => ({
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 240,
  },
  ...flex,
  icon: {
    position: 'absolute',
    left: 330,
    top: 15,
  },
});

class Tutor extends Component{
  componentWillUpdate () {
  }
  componentWillReceiveProps (newProps){
    const {session} = newProps;
    session.token&&this.props.history.push(`/main/${session.token}`)
  }
  render(){
    const { classes } = this.props;
    return (
      <div className={classes.flex_col}>
        <div style={{width:'100%'}}>
          <VideoPlayer paused={false} fill={true}>
            <source src="/reg.mp4" type='video/mp4'/>
          </VideoPlayer>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state){
  return {
    session: state.session,
  };
}
function matchDispatchToProps(dispatch){
  return bindActionCreators({
  },dispatch);
}

export default withRouter(connect(mapStateToProps,matchDispatchToProps)(withStyles(styles)(Tutor)));
