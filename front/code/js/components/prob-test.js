import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {orange, blue, grey } from '@material-ui/core/colors';
import { withRouter } from 'react-router-dom'
import { withStyles } from '@material-ui/core/styles';
import withWidth, { isWidthUp, isWidthDown } from '@material-ui/core/withWidth';

import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import Hidden from '@material-ui/core/Hidden';
import MobileStepper from '@material-ui/core/MobileStepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import StepContent from '@material-ui/core/StepContent';

import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';
import SentimentDissatisfied from '@material-ui/icons/SentimentDissatisfied';
import CheckCircle from '@material-ui/icons/CheckCircle';
import Typography from '@material-ui/core/Typography';

import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import SwipeableViews from 'react-swipeable-views';
import Divider from '@material-ui/core/Divider';

import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import MathJax from 'react-mathjax-preview'

import {flex} from '../../css/flex'
import {red,green} from '@material-ui/core/colors';

import {load,check} from '../actions/prob-tests-api';
import {select, restartProb, tic} from '../actions/prob-tests';

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing.unit * 2,
    margin: theme.spacing.unit,
    color: theme.palette.text.secondary,
  },
  button: {
    marginTop: theme.spacing.unit,
    marginRight: theme.spacing.unit,
  },
  icon: {
    margin: theme.spacing.unit,
    fontSize: 64,
  },
  ...flex,
  detailsHeader: {
    padding:0,
  },
  detailsBody: {
    flexDirection:"column",
    padding:0,
  },
  mathjax: {
    display:"inline-flex",
  },
  timer: {
    position: "absolute",
    width: "100%",
    top:70,
  },
  title:{
    marginTop:25
  },
});

class ProbTest extends Component{
  constructor(props){
    super(props);
  }
  componentWillMount() {
  }
  componentDidMount() {
    this.props.load(this.props.token,this.props.id)
    this.intervalID = setInterval(()=>this.tic(),1000)
  }
  componentWillUnmount() {
  }
  componentWillUpdate () {
  }
  componentDidUpdate(prevProps, prevState){
  }
  check(){
    clearInterval(this.intervalID)
    this.props.check(this.props.token, this.props.id,
      this.props.prob_tests.list.map((ex)=>({id:ex.id,ans:ex.answer})))
  }
  tic(){
    if(this.props.prob_tests.timer>0)
      this.props.tic();
    else 
      this.check();
  }
  restartProb(){
    this.props.restartProb();
    this.intervalID = setInterval(()=>this.props.tic(),1000)
  }
  render(){
    const { classes } = this.props;
    return (
      <div className={classes.root}>
        {!this.props.prob_tests.checked&&(
          <div>
            <Typography variant="caption" gutterBottom align="center" className={classes.timer}>
              {(parseInt(this.props.prob_tests.timer/3600)>0?`0${parseInt(this.props.prob_tests.timer/3600)}:`:'00:')+
                  (parseInt(this.props.prob_tests.timer/60)%60<10?'0':'')+
                  parseInt((this.props.prob_tests.timer%3600)/60).toString()+':'+
                  (this.props.prob_tests.timer%60<10?'0':'')+
                  (this.props.prob_tests.timer%60).toString()
              }
            </Typography>
            <Typography variant="title" align="center" className={classes.title}>{this.props.prob_tests.name}</Typography>
            {this.props.prob_tests.list.map((prob_test,index)=>(            
              prob_test.isinstruction==false ? (
                <Paper key={`prob_test_${prob_test.id}`} className={classes.paper} elevation={1}>
                  {prob_test.isformula?(<MathJax math={prob_test.question}/>): (prob_test.question&&prob_test.question.trim().length>0&&(
                    <Typography variant="subheading">
                      {prob_test.no}. {prob_test.question}
                    </Typography>)
                  )}
                  {prob_test.img != '' && (<img src={prob_test.img}/>)}
                  {prob_test.details !='' && (
                    <Typography>
                      {prob_test.details}
                    </Typography>)
                  }
                  <br/>
                  <Divider/>
                  <FormControl component="fieldset" className={classes.formControl}>
                    {prob_test.options.map((ans,index)=>(
                      <div>
                        <Radio
                          key={`ans_${index}`} value={ans.id.toString()}
                          checked={ans.id.toString() === prob_test.answer}
                          value={ans.id.toString()} 
                          onChange={(event)=>{this.props.select(prob_test.id,event.target.value)}}
                          name="radio-button-demo"
                          aria-label="A"
                        />
                        {ans.isformula?( <MathJax math={ans.value} className={classes.mathjax} />): ans.value}
                      </div>
                    ))}
                  </FormControl>
                </Paper>):(
                  <Paper key={`prob_test_${prob_test.id}`} className={classes.paper} elevation={1}>
                    {prob_test.isformula?(<MathJax math={prob_test.question}/>): (prob_test.question&&prob_test.question.trim().length>0&&(
                      <Typography variant="subheading">
                        {prob_test.question}
                      </Typography>)
                    )}
                    {prob_test.img != '' && (<img src={prob_test.img}/>)}
                    {prob_test.details !='' && (
                      <Typography variant="subheading">
                        {prob_test.details}
                      </Typography>)
                    }
                    <br/>
                  </Paper>
                )))
            }
            <Button variant="outlined" onClick={(event)=>this.check()} className={classes.button}>
              {this.props.session.lang==0?"Сынактын жыйынтыгы":"Завершить экзамен"}
            </Button>
          </div>
        )}

        {this.props.prob_tests.checked&&(
          <Paper square elevation={0} className={classes.flex_col}>
            {this.props.prob_tests.passed?<CheckCircle className={classes.icon} color="primary"/> : <SentimentDissatisfied className={classes.icon} color="secondary"/>}
            <Typography variant="headline" gutterBottom align="center" >
              {this.props.session.lang==0?"Урматтуу":"Уважаемый"} {this.props.session.fullname}!
            </Typography>
            <Typography variant="subheading" gutterBottom align="center" >
              {this.props.prob_tests.passed?(this.props.session.lang==0?"Сынак ийгиликтүү өтүлдү":"Тест успешно пройден!"):(this.props.session.lang==0?"Сынак өтүлгөн жок!":"Тест не пройден!")}
            </Typography>
            <Typography variant="subheading" gutterBottom align="center" >
              {this.props.session.lang==0?"Сынактын жыйынтыгы":"Ваш результат"}
            </Typography>
            <Typography variant="display1" gutterBottom align="center" >
              {this.props.prob_tests.score}% 
            </Typography>
            <Button variant="outlined" onClick={(event)=>this.restartProb()} className={classes.button}>
              {this.props.session.lang==0?"Тестти кайрадан тапшыруу":"Начать заново"}
            </Button>
          </Paper>
        )}

      </div>
    );
  }
}

function mapStateToProps(state,ownProps){
  return {
    id: ownProps.match.params.id,
    token: ownProps.match.params.token,
    session: state.session,
    prob_tests: state.prob_tests,
  };
}
function matchDispatchToProps(dispatch){
  return bindActionCreators({
    load,
    check,
    select,
    restartProb,
    tic,
  },dispatch);
}

export default withRouter(connect(mapStateToProps,matchDispatchToProps)(withWidth()((withStyles(styles)(ProbTest)))));
