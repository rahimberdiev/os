import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {orange, blue, grey } from '@material-ui/core/colors';
import { withRouter } from 'react-router-dom'
import { withStyles } from '@material-ui/core/styles';
import withWidth from '@material-ui/core/withWidth';

import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Hidden from '@material-ui/core/Hidden';

import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import GridListTileBar from '@material-ui/core/GridListTileBar';
import ListSubheader from '@material-ui/core/ListSubheader';
import IconButton from '@material-ui/core/IconButton';
import InfoIcon from '@material-ui/icons/Info';
import StarBorderIcon from '@material-ui/icons/StarBorder';

import {load} from '../actions/courses-api';
import {setCurrent} from '../actions/courses';
import {flex} from '../../css/flex';
import {getRes} from '../actions/api';

const styles = theme => ({
  ...flex,
  gridList: {
    width: '100%',
  },
  gridListTile: {
    cursor: "pointer",
  },
  titleBar: {
    background:
    'linear-gradient(to top, rgba(27,94,32,0.7) 0%, rgba(27,94,32,0.7) 80%, rgba(27,94,32,0) 100%)',
    height: 80,
  },
  titleBarStar: {
    background:'rgba(27,94,32,0.8)',
    height: 40,
  },
  title: {
    color: "white",
    whiteSpace: 'normal',
  },
  icon: {
    color: 'rgba(255, 255, 255, 0.54)',
  },
  iconStars: {
    color: 'yellow',
    width: 128,
    justifyContent: "flex-end",
    marginRight:4,
  },
});

class Subject extends Component{
  componentWillMount() {
    this.props.load(this.props.token,this.props.id)
  }
  componentWillUpdate () {
  }
  componentDidUpdate(prevProps, prevState){
    if(this.props.id!=prevProps.id){
      this.props.load(this.props.token,this.props.id)
    }
  }
  componentWillReceiveProps (newProps){
  }
  gridCols(width){
    switch(width){
      case 'xs':
      case 'sm':
        return 1;
        break;
      case 'md':
        return 2;
        break;
      case 'lg':
        return 2;
        break;
      case 'xl':
        return 3;
        break;
    }
  }
  select(course){
    this.props.setCurrent(course.id)
    this.props.history.push(`/main/${this.props.token}/course/${this.props.id}/${course.id}`)
  }

  render(){
    const { classes } = this.props;
    return (
      <div className="main">
        <div className={classes.flex_row_wrap}>
          <GridList cellHeight={400} cols={this.gridCols(this.props.width)} className={classes.gridList}>
            {this.props.courses.list.map(course=>
              <GridListTile key={`course_${course.id}`} cols={1} onClick={(event)=>{this.select(course)}} className={classes.gridListTile}>
                <img src={getRes(course.logo)} alt={course.name} />
                <GridListTileBar
                  titlePosition="top"
                  classes={{
                    root: classes.titleBarStar,
                  }}
                  actionIcon={
                    <IconButton className={classes.iconStars}>
                      {[...Array(course.level)].map((e,i)=>(<StarBorderIcon key={`star_${course.id}_${i}`} />))}
                    </IconButton>
                  }
                />
                <GridListTileBar
                  title={course.name}
                  classes={{
                    root: classes.titleBar,
                    title: classes.title,
                  }}
                  actionIcon={
                    <IconButton className={classes.icon}>
                      <InfoIcon />
                    </IconButton>
                  }
                />
              </GridListTile>
            )}
          </GridList>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state,ownProps){
  console.log(ownProps)
  return {
    id: ownProps.match.params.id,
    token: ownProps.match.params.token,
    session: state.session,
    subjects: state.subjects,
    courses: state.courses,
  };
}
function matchDispatchToProps(dispatch){
  return bindActionCreators({
    load:load,
    setCurrent: setCurrent,
  },dispatch);
}

export default withRouter(connect(mapStateToProps,matchDispatchToProps)(withStyles(styles)(withWidth()(Subject))));
