import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {orange, blue, grey, blueGrey } from '@material-ui/core/colors';
import { withRouter } from 'react-router-dom'
import { withStyles } from '@material-ui/core/styles';

import PropTypes from 'prop-types';
import AccountCircle from '@material-ui/icons/AccountCircle';
import AlternateEmail from '@material-ui/icons/AlternateEmail';
import Phone from '@material-ui/icons/Phone';
import VpnKey from '@material-ui/icons/VpnKey';

import InputAdornment from '@material-ui/core/InputAdornment';

import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';

import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import Checkbox from '@material-ui/core/Checkbox';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';


import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import MaskedInput from 'react-text-mask';
import NumberFormat from 'react-number-format';
import {changeUsername,changeFullname,changeLang,changeRegion,changeCity,changePassword,changePasswordConf,changeCode,setAgreed} from '../actions/sign-out'
import {signOut,approve} from '../actions/sign-out-api';
import {clear} from '../actions/sign-out';
import {loggedIn} from '../actions/session';
import {flex} from '../../css/flex';
import {load} from '../actions/regions-api';

const styles = theme => ({
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 240,
  },
  ...flex,
  icon: {
    position: 'absolute',
    right: 15,
    top: 15,
  },
});

class SignOut extends Component{
  componentWillMount() {
    this.props.load()
  }
  componentWillUpdate () {
  }
  componentWillReceiveProps (newProps){
    const {session} = newProps;
    session.token&&this.props.history.push(`/main/${session.token}`)
  }
  render(){
    const { classes } = this.props;
    return (
      <div className={classes.flex_col} style={{position:"relative",minHeight:750}}>
        <IconButton className={classes.icon} onClick={(event)=>this.props.history.push('/')}>
          <CloseIcon />
        </IconButton>
        {(this.props.sign_out.step==0)&&(
          <div className={classes.flex_col} style={{}}>
            <img src="/logo.jpg" style={{width:280}}></img>
            <br/>
            <Typography style={{color:"red"}} align="center">
              {this.props.sign_out.message}
            </Typography>
            <br/>
            <FormControl component="fieldset" style={{width:240}}>
              <FormLabel component="legend"><small>Окуу тили/Язык обучения</small></FormLabel>
              <RadioGroup
                aria-label="Gender"
                name="gender1"
                className={classes.group}
                value={this.props.sign_out.lang}
                onChange={event=>this.props.changeLang(parseInt(event.target.value))}
                style={{flexDirection:"row"}}
              >
                <FormControlLabel value={0} control={<Radio />} label="Кыр" />
                <FormControlLabel value={1} control={<Radio />} label="Рус" />
              </RadioGroup>
            </FormControl>
            <TextField
              id="id_username"
              label={this.props.sign_out.lang==0?"Телефон номурунуз":"Номер телефона"}
              autoFocus
              className={classes.textField}
              value={this.props.sign_out.username}
              onChange={event=>this.props.changeUsername(event.target.value)}
              margin="normal"
              error={this.props.sign_out.username_error}
              helperText={this.props.sign_out.username_error}
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start">
                    <Phone />
                  </InputAdornment>
                ),
              }}
            />
            <TextField
              id="id_fullname"
              label={this.props.sign_out.lang==0?"Аты жөнүнүз":"ФИО"}
              className={classes.textField}
              value={this.props.sign_out.fullname}
              onChange={event=>this.props.changeFullname(event.target.value)}
              margin="normal"
              error={this.props.sign_out.fullname_error}
              helperText={this.props.sign_out.fullname_error}
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start">
                    <AccountCircle />
                  </InputAdornment>
                ),
              }}
            />
            <FormControl className={classes.textField}>
              <InputLabel htmlFor="age-native-simple">{this.props.sign_out.lang==0?"Район/Шаар":"Район/Город"}</InputLabel>
              <Select
                native
                value={this.props.sign_out.region}
                onChange={(event) => this.props.changeRegion(event.target.value)}
                inputProps={{
                  name: 'region',
                  id: 'region-native-simple',
                }}
              >
                  <option value={-1}>не выбран</option>)
                {this.props.regions.list.map((r)=>
                  <option value={r.id}>{r.name}</option>)
                }
              </Select>
              <FormHelperText style={{color:"red"}}>{this.props.sign_out.region_error}</FormHelperText>
            </FormControl>
            <FormControl className={classes.textField}>
              <InputLabel htmlFor="age-native-simple">Айыл аймак</InputLabel>
              <Select
                native
                value={this.props.sign_out.city}
                onChange={(event) => this.props.changeCity(event.target.value)}
                inputProps={{
                  name: 'city',
                  id: 'city-native-simple',
                }}
              >
                  <option value={-1}>не выбран</option>)
                {(this.props.region ? this.props.region.cities : []).map((c)=>
                  <option value={c.id}>{c.name}</option>)
                }
              </Select>
            </FormControl>
            <TextField
              id="id_password"
          label={this.props.sign_out.lang==0?"Пароль":"Пароль"}
              className={classes.textField}
              value={this.props.sign_out.password}
              onChange={event=>this.props.changePassword(event.target.value)}
              margin="normal"
              error={this.props.sign_out.password_error}
              helperText={this.props.sign_out.password_error}
              type="password"
              autoComplete="current-password"
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start">
                    <VpnKey />
                  </InputAdornment>
                ),
              }}
            />
            <TextField
              id="id_password_conf"
          label={this.props.sign_out.lang==0?"Парольду кайталоо":"Подтверждение пароля"}
              className={classes.textField}
              value={this.props.sign_out.password_conf}
              onChange={event=>this.props.changePasswordConf(event.target.value)}
              margin="normal"
              error={this.props.sign_out.password_conf_error}
              helperText={this.props.sign_out.password_conf_error}
              type="password"
              autoComplete="current-password"
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start">
                    <VpnKey />
                  </InputAdornment>
                ),
              }}
            />
            <FormControlLabel className={classes.textField}
              control={
                <Checkbox
                  checked={this.props.sign_out.aggree}
                  onChange={(event)=>this.props.setAgreed(event.target.checked)}
                  color="primary"
                />
              }
              label={this.props.sign_out.lang==0?"Офертанын шарттары менен макулмун":"я согласен с условиями оферты"}
            />
            <br/>

            <Button variant="contained" color="primary" size="large" onClick={()=>{this.props.signOut(this.props.sign_out.username,this.props.sign_out.fullname,this.props.sign_out.lang,this.props.sign_out.password,this.props.sign_out.password_conf,this.props.sign_out.agreed,this.props.sign_out.region,this.props.sign_out.city)}}>
          {this.props.sign_out.lang==0?"Системага каттоо":"Зарегистрироваться"}
            </Button>
            <br/>
          </div>)
        }
        {(this.props.sign_out.step==1)&&(
          <div className={classes.flex_col} style={{}}>
            <IconButton className={classes.icon} onClick={(event)=>this.props.history.push('/')}>
              <CloseIcon />
            </IconButton>
            <img src="/logo.jpg" style={{width:280}}></img>
            <br/>
            <Typography style={{color:"red"}} align="center">
              {this.props.sign_out.message}
            </Typography>
            <br/>
            <TextField
              id="id_approve_code"
              label={this.props.sign_out.lang==0?"Жашыруун SMS код":"Код подтверждения"}
              className={classes.textField}
              value={this.props.sign_out.code}
              onChange={event=>this.props.changeCode(event.target.value)}
              margin="normal"
              error={this.props.sign_out.code_error}
              helperText={this.props.sign_out.code_error}
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start">
                    <VpnKey />
                  </InputAdornment>
                ),
              }}
            />
            <Button variant="contained" color="primary" size="large" onClick={()=>{this.props.approve(this.props.sign_out.username,this.props.sign_out.code,this.props.sign_out.lang)}}>
              {this.props.sign_out.lang==0?"Мен экенимди тастыктоо":"Подтвердить"} 
            </Button>
            <br/>
          </div>
        )}
        {(this.props.sign_out.step==2)&&(
          <div className={classes.flex_col} style={{}}>
            <IconButton className={classes.icon} onClick={(event)=>this.props.history.push('/')}>
              <CloseIcon />
            </IconButton>
            <img src="/logo.jpg" style={{width:280}}></img>
            <br/>
            <Typography style={{color:"green"}} align="center" variant="headline">{this.props.sign_out.lang==0?"Куттуктайбыз! Сиз ийгиликтүү катталдыңыз!":"Поздравляем! Вы успешно зарегистрировались!"}
            </Typography>
            <br/>
            <br/>
            <Button variant="contained" color="primary" size="large" onClick={()=>{this.props.loggedIn({
              token:this.props.sign_out.token,
              token_created_at:this.props.sign_out.token_created_at,
              fullname:this.props.sign_out.fullname,
              username:this.props.sign_out.username,
              payments:this.props.sign_out.payments,
              full_access:this.props.sign_out.full_access});this.props.clear()}}>
              {this.props.sign_out.lang==0?"Улантуу":"Продолжить"}
            </Button>
            <br/>
        </div>
        )}
        <br/>
      </div>
    );
  }
}

function mapStateToProps(state){
  return {
    regions: state.regions,
    region: state.regions.list.find(region=>region.id == state.sign_out.region),
    sign_out: state.sign_out,
    session: state.session,
  };
}
function matchDispatchToProps(dispatch){
  return bindActionCreators({
    changeUsername,
    changeFullname,
    changeLang,
    changeRegion,
    changeCity,
    changePassword,
    changePasswordConf,
    changeCode,
    setAgreed,
    signOut,
    approve,
    loggedIn,
    clear,
    load,
  },dispatch);
}

export default withRouter(connect(mapStateToProps,matchDispatchToProps)(withStyles(styles)(SignOut)));
