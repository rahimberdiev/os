import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {orange, blue, grey } from '@material-ui/core/colors';
import { withRouter } from 'react-router-dom'
import { withStyles } from '@material-ui/core/styles';
import withWidth from '@material-ui/core/withWidth';
import {load} from '../actions/subjects-api';

import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Hidden from '@material-ui/core/Hidden';

import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import GridListTileBar from '@material-ui/core/GridListTileBar';
import ListSubheader from '@material-ui/core/ListSubheader';
import IconButton from '@material-ui/core/IconButton';
import InfoIcon from '@material-ui/icons/Info';

import {flex} from '../../css/flex';
import {getRes} from '../actions/api';

const styles = theme => ({
  ...flex,
  gridList: {
    width: '100%',
  },
  gridListTile: {
    cursor: "pointer",
  },
  titleBar: {
    background:
    'linear-gradient(to top, rgba(27,94,32,0.7) 0%, rgba(27,94,32,0.7) 60%, rgba(27,94,32,0) 100%)',
    height: 80,
  },
  title: {
    color: "white",
    whiteSpace: 'normal',
  },
  icon: {
    color: 'rgba(255, 255, 255, 0.54)',
  },
});

class Subjects extends Component{
  componentWillMount() {
    if(!this.props.token||this.props.token==='')
      this.props.history.push('/')
    if(this.props.subjects.length===0)
      this.props.load(this.props.token)
  }
  componentWillUpdate () {
  }
  componentDidUpdate(prevProps, prevState){
  }
  componentWillReceiveProps (newProps){
  }
  gridCols(width){
    switch(width){
      case 'xs':
      case 'sm':
        return 1;
        break;
      case 'md':
        return 2;
        break;
      case 'lg':
        return 2;
        break;
      case 'xl':
        return 3;
        break;
    }
  }
  select(subject){
    this.props.history.push(`${this.props.match.url}/subject/${subject.id}`)
  }

  render(){
    const { classes } = this.props;
    return (
      <div className="main">
        <div className={classes.flex_row_wrap}>
          <GridList cellHeight={400} cols={this.gridCols(this.props.width)} className={classes.gridList}>
            {this.props.subjects.list.map(subject=>
              <GridListTile key={`subject_${subject.id}`} cols={1} onClick={(event)=>{this.select(subject)}} className={classes.gridListTile}>
                <img src={getRes(subject.logo)} alt={subject.name} />
                <GridListTileBar
                  title={subject.name}
                  classes={{
                    root: classes.titleBar,
                    title: classes.title,
                  }}
                  actionIcon={
                    <IconButton className={classes.icon}>
                      <InfoIcon />
                    </IconButton>
                  }
                />
              </GridListTile>
            )}
          </GridList>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state,ownProps){
  return {
    id: ownProps.match.params.id,
    token: ownProps.match.params.token,
    session: state.session,
    subjects: state.subjects,
  };
}
function matchDispatchToProps(dispatch){
  return bindActionCreators({
  },dispatch);
}

export default withRouter(connect(mapStateToProps,matchDispatchToProps)(withStyles(styles)(withWidth()(Subjects))));
