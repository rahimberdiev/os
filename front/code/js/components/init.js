import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {orange, blue, grey, blueGrey } from '@material-ui/core/colors';
import { withRouter } from 'react-router-dom'
import { withStyles } from '@material-ui/core/styles';

import Grid from '@material-ui/core/Grid';
import Snackbar from '@material-ui/core/Snackbar';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Typography from '@material-ui/core/Typography';

import Dialog from '@material-ui/core/Dialog';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Slide from '@material-ui/core/Slide';

import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import PlayCircleOutlineIcon from '@material-ui/icons/PlayCircleOutline';

import { Route, Link, Switch } from "react-router-dom";

import {flex} from '../../css/flex';

import SignIn from './sign-in'
import SignOut from './sign-out'
import ForgotPassword from './forgot-password'
import {closeSnack} from '../actions/snack'
import {openDialog,closeDialog} from '../actions/init'

const styles = theme => ({
  ...flex,
  banner:{
    backgroundImage:'url("/banner3.jpeg")',
    backgroundColor: '#cccccc',
    backgroundPosition: 'center',
    backgroundRepeat: 'no-repeat',
    backgroundSize: 'cover',
    position: 'relative',
  },
  h1:{
    background: "rgba(0,0,0,0.5)",
    borderRadius: 30,
    padding: 10,

  },
  imgBox:{
    width: 280,
    margin: "auto",
    marginBottom:20,
    display: "block",
    borderRadius: 20,
    cursor:"pointer",
  },
  typo:{
    marginBottom:15,
  },
  dialogContent:{
    [theme.breakpoints.down('sm')]: {
      margin: "100px 10px 10px 10px",
    },
    [theme.breakpoints.up('md')]: {
      margin: "100px 50px 50px 50px",
    },
    [theme.breakpoints.up('lg')]: {
      margin: "100px",
    },
  }
});
function Transition(props) {
  return <Slide direction="up" {...props} />;
}

class Init extends Component{
  componentWillUpdate () {
  }
  componentWillReceiveProps (newProps){
    //const {token} = newProps;
    //token&&
  }
  handleClose(reason) {
    if (reason === 'clickaway') {
      return;
    }
    this.props.closeSnack();
  };
  render(){
    const { classes } = this.props;
    return (
      <Grid container spacing={0} style={{height:'100%'}}>
        <Grid item xs={12} sm={6} md={4} lg={4} xl={3}>
          <Switch>
            <Route exact path={`${this.props.match.url}/`} component={SignIn}/>
            <Route exact path={`${this.props.match.url}/signin`} component={SignIn}/>
            <Route exact path={`${this.props.match.url}/signout`} component={SignOut}/>
            <Route exact path={`${this.props.match.url}/forgot`} component={ForgotPassword}/>
          </Switch>
          <Snackbar
            anchorOrigin={{
              vertical: 'bottom',
              horizontal: 'left',
            }}
            open={this.props.snack.open}
            autoHideDuration={3000}
            onClose={(event,reason)=>this.handleClose(reason)}
            ContentProps={{
              'aria-describedby': 'message-id',
            }}
            message={<span id="message-id">{this.props.snack.message}</span>}
            action={[
              <IconButton
                key="close"
                aria-label="Close"
                color="inherit"
                className={classes.close}
                onClick={(event,reason)=>this.handleClose(reason)}
              >
                <CloseIcon />
              </IconButton>,
            ]}
          />
        </Grid>
        <Grid item xs={12} sm={6} md={8} lg={8} xl={9}>
          <div className={classes.flex_col} style={{minHeight:600}}>
            <a href="https://wa.me/996700854775">
              {this.props.init.lang===0 ? (
                <img src="/wa_kg.png" className={classes.imgBox}/>):(
                  <img src="/wa_ru.png" className={classes.imgBox}/>)
              }
            </a>
            <a href={this.props.init.lang===0?"reg_kg.mp4":"reg_ru.mp4"}>
              {this.props.init.lang===0 ? (
                <img src="/reg_kg.png" className={classes.imgBox}/>):(
                  <img src="/reg_ru.png" className={classes.imgBox}/>)
              }
            </a>
            <a href={this.props.init.lang===0?"pay_kg.mp4":"pay_ru.mp4"}>
              {this.props.init.lang===0 ? (
                <img src="/pay_kg.png" className={classes.imgBox}/>):(
                  <img src="/pay_ru.png" className={classes.imgBox}/>)
              }
            </a>
            <a onClick={(event)=>this.props.openDialog(0)}>
              {this.props.init.lang===0 ? (
                <img src="/info_kg.png" className={classes.imgBox}/>):(
                  <img src="/info_ru.png" className={classes.imgBox}/>)
              }
            </a>
            <Dialog
              fullScreen
              open={this.props.init.dialog_open}
              onClose={(event)=>this.props.closeDialog()}
              TransitionComponent={Transition}
            >
              <AppBar className={classes.appBar}>
                <Toolbar>
                  <IconButton color="inherit" onClick={(event)=>this.props.closeDialog()} aria-label="Close">
                    <CloseIcon />
                  </IconButton>
                  <img src="/apgrade_white.png" style={{width:120}}></img>
                </Toolbar>
              </AppBar>
              {this.props.init.dialog_content===0 && (
                <div className={classes.dialogContent}
                >
                  <Typography variant="title" gutterBottom className={classes.typo}>
                    Урматтуу достор, окуучулар жана мугалимдер!
                  </Typography>
                  <br/>
                  <Typography variant="body1" gutterBottom className={classes.typo}>
                    Жалпы Республикалык Тест (ЖРТ) бул жогорку окуу жайларына кирүү үчүн бирден – бир  жол болуп, тестке ар бир мектеп бүтүрүүчүсү өз каалоосу менен кире ала тургандай түзүлгөн. ЖРТны тапшыруу  менен ар бир Кыргызстандык жаран жогорку окуу жайларына кирүү мүмкүнчүлүгүнө ээ болот. Тесттин жыйынтыктарына карап бюджеттик, контрактык негизде окуу мүмкүнчүлүгү каралган. Тактап айта турган болсок алган упайына карап, бюджеттик, контрактык орундарга өтүүгө, адилеттүү бирдей шарт түзүлгөн.
                  </Typography>
                  <Typography variant="body1" gutterBottom className={classes.typo}>
                    ЖРТ  (ОРТ) – окуучунун мектепте алган жалпы билимин, кошумча логикалык ой – жүгүртүүсүн, билим жөндөмдүүлүгүн аныктоочу сынак. Албетте, сынакты ийгиликтүү тапшырып,  андан  жогорку балл алуу үчүн эң мыкты даярдануу керек. ЖРТ окуу куралдарында берилген теориялык эреже – түшүндүрмөлөрдү, окуучунун жаттап алган билимин, чыгармачыл жөндөмүн, каалоо тилегин эмес, жөн гана окуучунун ой – жүгүртүү, чечим чыгаруу, түшүнө билүүсүн, шыктуулугун текшерет. 
                  </Typography>
                  <Typography variant="body1" gutterBottom className={classes.typo}>
                    Күнүбүздө көптөгөн окуучулар сынакка жакшы даярдана албай ЖОЖдорго (угиверситеттерге) кирүү кыялынан ажырап калып жатат. Бул нерсеге  мектепте берилген билимди толук өздөштүрө албай калып жатканын, ЖРТ боюнча маалымат түшүнүк аз экенин жүйө кылып көрсөтүп, ар кандай кошумча курстарга барып, билимин толуктоого аракет кылып келгендер арбын. Мектептен сырткары кошумча курсттардан алынган билим, тесттеги жыйынтык упайга түздөн – түз таасир этип жаткандыгына байланыштуу теңсиздик, материалдык мүмкүнчүлүктү алдына сүрөп чыгып баратканы бираз туура эместей сезилет. 
                  </Typography>
                  <Typography variant="body1" gutterBottom className={classes.typo}>
                    Биз туулган жерибизге пайдалуу болуу үчүн, караңгы түндү жарык кылуу максатында, келечек бир гана билимдүү жаштар менен курулушуна ишенгенибиз үчүн, мамлекетибизге жана келечектин ээлери болгон жаш – муундарга пайдалуу болсом экен деген жаштар менен биргеликте apgrade.kg  окуу системасын иштеп чыктык.
                  </Typography>
                  <Typography variant="body1" gutterBottom className={classes.typo}>
                    apgrade.kg  бул жалпы республикалык тестке даярдоо сайт-приложениеси болуп, аны бүтүндөй мекенибиздин булуң – бурчунда, ар бир окуучу, ар кандай шартта болбосун  көрө ала тургандай кылып иштелип чыкты. Сайт – приложенин ичинде, Негизги тест (ЖРТ)  боюнча баардык бөлүмдөргө даярдануу мүмкүнчүлүгү каралган. Каттоодон өткөн ар бир окуучу орус  жана кыргыз тилдеринде, тилдин грамматикалык практикасынан, математика боюнча, сөздүк вербалдык бөлүмдөрдөн аналогия, окуу түшүнүү, сүйлөмдөрдү толуктоо боюнча даярдана алат. Ар бир бөлүм боюнча видео сабактар, темага байланыштуу тапшырмалар, тема боюнча экзамендик тесттерди көрүп, сынамык тесттерди иштөө мүмкүнчүлүгү каралган. 
                  </Typography>
                  <Typography variant="body1" gutterBottom className={classes.typo}>
                    Математикадан тестте кездешүүчү суроолор боюнча темалар каралып, сабактар өтүлүп, негизги тестте кездешүүсү мүмкүн болгон суроолорго окшош суроолор жүктөлгөн. Аналогия бөлүмдө сөздүк окшоштуктар, айырмачылыктар, байланыштар каралып, практика кылуу үчүн суроолор камтылган. Окуу түшүнүүдө бат окууну, окуган маалыматка анализ жана синтез жүргүзө билүүгө үйрөтүүчү сабактар бар. Грамматика боюнча лексика, морфологияга тиешелүү темалар боюнча тесттер жазылган.
                  </Typography>
                  <Typography variant="body1" gutterBottom className={classes.typo}>
                    Бул эмгектин (apgrade.kg )  ичиндеги маалыматтар бүтүндөй мектеп окуучуларына, Нарын, Талас, Баткен, Ош ж.б  бардык региондорго, ар бир айылга, ар бир үйгө, ар бир окуучуга бирдей жетип, аларга билим өздөштүрүүдө, ЖОЖго кирүүдө көмөкчү болот деген чоң ишеничибиз бар, жана үмүтүбүз чоң. Жалпы республикалык тестирлөөнү тапшырууга даярдануучу окуучуларга ийгилик каалап келечек үчүн пайдалуу инсан, мыкты адис болуусуна тилектешпиз!
                  </Typography>
                  <br/>
                  <Typography variant="title" gutterBottom className={classes.typo}>
                    Урматтоо менен apgrade.kg жамааты.
                  </Typography>
                </div>
              )}
            </Dialog>
          </div>
        </Grid>
      </Grid>
    );
  }
}

function mapStateToProps(state){
  return {
    snack: state.snack,
    init: state.init,
  };
}
function matchDispatchToProps(dispatch){
  return bindActionCreators({
    closeSnack:closeSnack,
    openDialog,closeDialog,
  },dispatch);
}

export default withRouter(connect(mapStateToProps,matchDispatchToProps)(withStyles(styles)(Init)));
