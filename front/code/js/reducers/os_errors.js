const initialState = {
  show: false,
  message: '',
}
export default function(state=initialState,action){
  switch(action.type){
    case "OPEN_SNACKBAR": 
      return {...state, ...{show:true,message:action.payload}};
      break;
    case "CATEGORIES_LOAD_FAILED": 
      return {...state, ...{show:true,message:action.payload}};
      break;
    case "CLOSE_SNACKBAR":
      return {...state, ...{show:false,message:''}};
      break;
    default:
      return state;
  } 
}
