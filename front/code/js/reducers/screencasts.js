const initialState = {
  list: [],
  course:{id:-1,name:""},
  active_sc_index:-1,
}
export default function(state=initialState,action){
  switch(action.type){
    case "SCREENCASTS_LOADED":
      return {...initialState,...action.payload};
      break;
    case "SCREENCASTS_ACTIVATED":
      return {...state, ...action.payload};
      break;
    case "SCREENCASTS_NEXT_SC":
      return {...state, ...{active_sc_index:action.payload+1}};
      break;
    case "SCREENCASTS_PREV_SC":
      return {...state, ...{active_sc_index:action.payload-1}};
      break;
    default:
      return state;
  } 
}
