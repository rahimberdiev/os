const initialState = {
  category:0,
  m_anchor:null,
  m_opened:false,
  m_items:{
    1: "Предметы",
    2: "Курсы",
    3: "Скринкасты",
    4: "Тесты"
  },
  m_item:1,

  p_anchor:null,
  p_opened:false,
  p_items:{
    3: {name: "Мой профиль",url:"/profile"},
    4: {name: "Мой статус",url:"/status"},
    5: {name: "Выход",url:"/logout"}
  },
  p_item:1,
  sidenav_opened: false,
}

export default function(state=initialState,action){
  switch(action.type){
    case "MOPEN": 
      return Object.assign({...state,...{m_opened:true,m_anchor:action.payload}});
      break;
    case "MCLOSE": 
      return Object.assign({...state,...{m_opened:false,m_item:action.payload}});
      break;
    case "POPEN": 
      return Object.assign({...state,...{p_opened:true,p_anchor:action.payload}});
      break;
    case "PCLOSE": 
      return Object.assign({...state,...{p_opened:false,p_item:action.payload}});
      break;
    case "SIDENAV_OPEN": 
      return Object.assign({...state,...{sidenav_opened:true}});
      break;
    case "SIDENAV_CLOSE": 
      return Object.assign({...state,...{sidenav_opened:false}});
      break;
    default:
      return state;
  } 
}
