const initialState = {
  step:0,
  lang:0,
  username:'',
  username_error: '',
  password:'',
  password_error:'',
  password_conf:'',
  password_conf_error:'',
  pin:'',
  pin_error:'',
  message:'',
  fullname:'',
  token:'',
  token_created_at:'',
}
export default function(state=initialState,action){
  switch(action.type){
    case "FORGOT_USERNAME_CHANGED": 
      return {...state, ...{username: action.payload}};
      break;
    case "FORGOT_PASSWORD_CHANGED": 
      return {...state, ...{password: action.payload}};
      break;
    case "FORGOT_PASSWORD_CONF_CHANGED": 
      return {...state, ...{password_conf: action.payload}};
      break;
    case "FORGOT_PIN_CHANGED": 
      return {...state, ...{pin:action.payload}};
      break;
    case "FORGOT_LANG_CHANGED": 
      return {...state, ...{lang:action.payload}};
      break;
    case "FORGOT_PIN_SENT": 
      return {...state, ...{step:1,message:''}};
      break;
    case "FORGOT_APPROVED": 
      return {...state, ...{step:2,message:''},...action.payload};
      break;
    case "FORGOT_CHANGE_FAILED": 
      return {...state, ...{message:action.payload}};
      break;
    case "FORGOT_CLEAR": 
      return initialState;
      break;
    case "FORGOT_VALIDATE": 
      return {...state, ...{
        password_error: state.password?"":(state.lang==0?"Парольду киргизиңиз":"Укажите пароль"),
        password_conf_error: state.password_conf==state.password?"":(state.lang==0?"Парольдор дал келиши зарыл":"Пароли не совпадают"),
        pin_error: state.pin?"":(state.lang==0?"Жашыруун SMS кодду киргизиңиз":"Введите секретный SMS код"),
      }}
      break;
    case "FORGOT_VALIDATE_USERNAME": 
      return {...state, ...{
        username_error: state.username?(state.username.trim().length==10?"":(state.lang==0?"Көрсөтүлгөн тел.номур туура эмес":"Не верный номер телефона")):(state.lang==0?"Телефон номуруңузду көрсөтүңүз":"Укажите номер телефона"),
      }}
      break;
    default:
      return state;
  } 
}
