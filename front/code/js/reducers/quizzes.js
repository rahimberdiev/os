const initialState = {
  list: [],
  index:0,
  open: false,
  checked: false,
  score:0,
  expanded:false,
}
export default function(state=initialState,action){
  switch(action.type){
    case "QUIZZES_OPEN":
      return {...initialState, ...{open:true,list:state.list.map((q)=>({...q,...{answer:null}}))}};
      break;
    case "QUIZZES_CLOSE":
      return {...state, ...{open:false}};
      break;
    case "QUIZZES_LOADED":
      return {...initialState, ...{list:action.payload}};
      break;
    case "QUIZZES_MOVE_NEXT":
      return {...state, ...{index:state.index<state.list.length-1?state.index+1:state.index,expanded:false}};
      break;
    case "QUIZZES_MOVE_BACK":
      return {...state, ...{index:state.index>0?state.index-1:0,expanded:false}};
      break;
    case "QUIZZES_SELECT":
      return {...state, ...{list:state.list.map((quiz)=>{
        return quiz.id===action.payload.quiz_id?
          {...quiz,...{answer:action.payload.answer}}:
          quiz 
      })}};
      break;
    case "QUIZZES_CHECKED":
      return {...state, ...{checked:true},...action.payload};
      break;
    case "QUIZZES_EXPANDED":
      return {...state, ...{expanded:action.payload}};
      break;

    default:
      return state;
  } 
}
