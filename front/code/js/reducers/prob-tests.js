const initialState = {
  list: [],
  name:'',
  duration:0,
  index:0,
  checked:false,
  passed:false,
  score:0,
  timer:0,
}
export default function(state=initialState,action){
  switch(action.type){
    case "PROB_TESTS_LOADED":
      return {...initialState, ...action.payload,...{timer:action.payload.duration*60}};
      break;
    case "PROB_TESTS_SELECT":
      return {...state, ...{list:state.list.map((prob_test)=>{
        return prob_test.id===action.payload.prob_test_id?
          {...prob_test,...{answer:action.payload.answer}}:
          prob_test 
      })}};
      break;
    case "PROB_TESTS_RESTART":
      return {...state, ...{list:state.list.map( (prob_test) => { return {...prob_test,...{answer:null}}}), checked: false, passed: false, score: 0, index: 0, timer: state.duration*60}};
      break;
    case "PROB_TESTS_CHECKED":
      return {...state, ...{checked:true},...action.payload};
      break;
    case "PROB_TESTS_TIC":
      return {...state, ...{timer: (state.timer<=0?0:state.timer-1)}};
      break;
    default:
      return state;
  } 
}
