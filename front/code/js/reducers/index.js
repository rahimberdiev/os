import {combineReducers} from 'redux'
import { routerReducer } from 'react-router-redux'
import sessionReducer from './session.js'
import menuReducer from './menu.js'
import subjectsReducer from './subjects.js'
import coursesReducer from './courses.js'
import sectionsReducer from './sections.js'
import screencastsReducer from './screencasts.js'
import quizzesReducer from './quizzes.js'
import signOutReducer from './sign-out.js'
import snackReducer from './snack.js'
import examsReducer from './exams.js'
import regionsReducer from './regions.js'
import forgotPasswordReducer from './forgot-password.js'
import accessInfoReducer from './access-info.js'
import probTestsReducer from './prob-tests.js'
import probsReducer from './probs.js'
import initReducer from './init.js'

const allReducers = combineReducers({
  router: routerReducer,
  sign_out: signOutReducer,
  session: sessionReducer,
  menu: menuReducer,
  subjects: subjectsReducer,
  courses: coursesReducer,
  sections: sectionsReducer,
  screencasts: screencastsReducer,
  quizzes: quizzesReducer,
  exams: examsReducer,
  regions: regionsReducer,
  snack: snackReducer,
  forgot_password: forgotPasswordReducer,
  access_info: accessInfoReducer,
  prob_tests: probTestsReducer,
  probs: probsReducer,
  init: initReducer,
});

export default allReducers
