const initialState = {
  list: [],
}
export default function(state=initialState,action){
  switch(action.type){
    case "PROBS_LOADED":
      return {...initialState, ...action.payload};
      break;
    default:
      return state;
  }
}
