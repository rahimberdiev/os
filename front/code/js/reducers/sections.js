const initialState = {
  list: [],
  selected_id :-1,
  active_id :-1,
  active_sc_id:-1,
}
export default function(state=initialState,action){
  switch(action.type){
    case "SECTIONS_LOADED":
      return {...initialState,...{selected_id:action.payload.active_id}, ...action.payload};
      break;
    case "SECTIONS_MOVED":
      return {...state, ...{selected_id:action.payload.active_id},...action.payload};
      break;
    case "SECTIONS_SELECTED":
      return {...state, ...{selected_id:action.payload}};
      break;
    case "SECTIONS_ACTIVATED":
      return {...state, ...action.payload};
      break;
    default:
      return state;
  } 
}
