const initialState = {
  list: [],
}
export default function(state=initialState,action){
  switch(action.type){
    case "SUBJECTS_LOADED":
      return {...initialState, ...{list:action.payload}};
      break;
    default:
      return state;
  } 
}
