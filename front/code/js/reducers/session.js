const initialState = {
  user_id:-1,
  username:'',
  username_error: '',
  fullname:'',
  password:'',
  password_error:'',
  token:'',
  token_created_at:'',
  lang:0,
  payments: 0,
  full_access: false,
  message:'',
  access_denied: false,
}
export default function(state=initialState,action){
  switch(action.type){
    case "SESSION_VALIDATE": 
      return {...state,...{
        username_error: state.username?(state.username.trim().length==10?"":(state.lang==0?"Берилген тел.номур туура эмес":"Не верный номер телефона")):(state.lang==0?"Телефон номуруңузду киргизиңиз":"Укажите номер телефона"),
        password_error: state.password?"":(state.lang==0?"Парольду киргизиңиз":"Укажите пароль"),
      }}
      break;
    case "SESSION_LOGGED_IN": 
      return {...state, ...action.payload,...{access_denied:false,password:'',message:''}};
      break;
    case "SESSION_LOGIN_FAILED": 
      return {...state, ...{message:action.payload,access_denied:true}};
      break;
    case "SESSION_LOGGED_OUT": 
      return {...initialState,...{access_denied:true}}
      break;
    case "SESSION_USERNAME_CHANGED": 
      return {...state, ...{username: action.payload}};
      break;
    case "SESSION_LANG_CHANGED": 
      return {...state, ...{lang: action.payload}};
      break;
    case "SESSION_PASSWORD_CHANGED": 
      return {...state, ...{password: action.payload}};
      break;
    default:
      return state;
  } 
}
