const initialState = {
  list: [],
}
export default function(state=initialState,action){
  switch(action.type){
    case "REGIONS_LOADED":
      return {...initialState, ...{list:action.payload}};
      break;
    default:
      return state;
  } 
}
