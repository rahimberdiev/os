const initialState = {
  open: false,
}
export default function(state=initialState,action){
  switch(action.type){
    case "ACCESS_INFO_OPEN": 
      return {...state, ...{open:true}};
      break;
    case "ACCESS_INFO_CLOSE": 
      return {...state, ...{open:false}};
      break;
    default:
      return state;
  } 
}
