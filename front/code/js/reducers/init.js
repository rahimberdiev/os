const initialState = {
  dialog_open: false,
  dialog_content:0,
  lang:0,
}
export default function(state=initialState,action){
  switch(action.type){
    case "INIT_DIALOG_OPEN": 
      return {...state, ...{dialog_open:true,dialog_content:action.payload}};
      break;
    case "INIT_DIALOG_CLOSE": 
      return {...state, ...{dialog_open:false}};
      break;
    case "SIGN_OUT_LANG_CHANGED": 
    case "SESSION_LANG_CHANGED": 
      return {...state, ...{lang: action.payload}};
      break;
    default:
      return state;
  } 
}
