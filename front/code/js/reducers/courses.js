const initialState = {
  list: [],
  current_id:-1,
}
export default function(state=initialState,action){
  switch(action.type){
    case "COURSES_LOADED":
      return {...initialState, ...{list:action.payload}};
      break;
    case "COURSES_SET_CURRENT":
      return {...state, ...{current_id:action.payload}};
      break;
    default:
      return state;
  } 
}
