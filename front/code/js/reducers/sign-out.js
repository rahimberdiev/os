const initialState = {
  username:'',
  username_error:'',
  password:'',
  password_error:'',
  password_conf:'',
  password_conf_error:'',
  fullname:'',
  fullname_error:'',
  lang:0,
  region:-1,
  region_error:'',
  city:-1,
  agreed: false,
  agreed_error:'',
  lang_error:'',
  token:'',
  token_created_at:'',
  message:'',
  step:0,
  code:'',
}
export default function(state=initialState,action){
  switch(action.type){
    case "SIGN_OUT_USERNAME_CHANGED": 
      return {...state, ...{username: action.payload}};
      break;
    case "SIGN_OUT_FULLNAME_CHANGED": 
      return {...state, ...{fullname: action.payload}};
      break;
    case "SIGN_OUT_LANG_CHANGED": 
      return {...state, ...{lang: action.payload}};
      break;
    case "SIGN_OUT_REGION_CHANGED": 
      return {...state, ...{region: action.payload}};
      break;
    case "SIGN_OUT_CITY_CHANGED": 
      return {...state, ...{city: action.payload}};
      break;
    case "SIGN_OUT_PASSWORD_CHANGED": 
      return {...state, ...{password: action.payload}};
      break;
    case "SIGN_OUT_PASSWORD_CONF_CHANGED": 
      return {...state, ...{password_conf: action.payload}};
      break;
    case "SIGN_OUT_AGREED_CHANGED": 
      return {...state, ...{agreed: action.payload}};
      break;
    case "SIGN_OUT_VALIDATE": 
      return {...state, ...{
        username_error: state.username?(state.username.trim().length==10?"":(state.lang==0?"Териглен телефон номур туура эмес":"Не верный номер телефона")):(state.lang==0?"Телефон номуруңузду киргизиңиз":"Укажите номер телефона"),
        password_error: state.password?"":(state.lang==0?"Парольду киргизиңиз":"Укажите пароль"),
        password_conf_error: state.password_conf==state.password?"":(state.lang==0?"Парольдор дал келиши зарыл":"Пароли не совпадают"),
        fullname_error: state.fullname?"":(state.lang==0?"Аты жөнүнүңүздү киргизиңиз":"Укажите ФИО"),
        region_error: state.region!=-1?"":(state.lang==0?"Районду же шаарды тандагыныз":"Укажите ройон или город"),
        agreed_error: state.agreed?"":(state.lang==0?"Катталуу үчүн офертанын шарттары менен макул болууңуз шарт":"Регистрация невозможна без согласия с условиями оферты"),
        message: state.agreed?"":(state.lang==0?"Катталуу үчүн офертанын шарттары менен макул болууңуз шарт":"Регистрация невозможна без согласия с условиями оферты"),
      }}
      break;
    case "SIGN_OUT_CLEAR":
      return initialState;
    case "SIGNED_OUT":
      return {...state, ...{step: 1,message:''}};
    case "SIGN_OUT_APPROVED":
      return {...state, ...{step: 2,message:''},...action.payload};
    case "SIGN_OUT_FAILED":
      return {...state, ...{message: action.payload}};
    case "SIGN_OUT_STEP_CHANGED":
      return {...state, ...{step: action.payload,message:''}}
    case "SIGN_OUT_CODE_CHANGED":
      return {...state, ...{code: action.payload}}

    default:
      return state;
  } 
}
