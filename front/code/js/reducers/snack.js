const initialState = {
  open: false,
  message: '',
}
export default function(state=initialState,action){
  switch(action.type){
    case "SNACK_OPEN": 
      return {...state, ...{open:true,message:action.payload}};
      break;
    case "SNACK_CLOSE": 
      return {...state, ...{open:false,message:''}};
      break;
    default:
      return state;
  } 
}
