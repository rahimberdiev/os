const initialState = {
  list: [],
  index:0,
  checked:false,
  passed:false,
  score:0,
}
export default function(state=initialState,action){
  switch(action.type){
    case "EXAMS_LOADED":
      return {...initialState, ...{list:action.payload}};
      break;
    case "EXAMS_SELECT":
      return {...state, ...{list:state.list.map((exam)=>{
        return exam.id===action.payload.exam_id?
          {...exam,...{answer:action.payload.answer}}:
          exam 
      })}};
      break;
    case "EXAMS_CHECKED":
      return {...state, ...{checked:true},...action.payload};
      break;
    case "EXAMS_EXPANDED":
      return {...state, ...{list:state.list.map((exam)=>{
        return exam.id===action.payload?
          {...exam,...{expanded: !exam.expanded}}:
          exam 
      })}};
      break;
    default:
      return state;
  } 
}
