import React from 'react';
import ReactDOM from "react-dom";

import { Provider } from "react-redux";
import { ConnectedRouter as Router, routerReducer, routerMiddleware } from "react-router-redux";
import { Route, Link, Switch } from "react-router-dom";
import { createStore, applyMiddleware, combineReducers } from "redux";

import { composeWithDevTools } from 'redux-devtools-extension';
import Main from './components/main';
import Subject from './components/subject';
import Course from './components/course';
import Init from './components/init';
import Tutor from './components/tutor'
import thunk from "redux-thunk";
import createHistory from 'history/createBrowserHistory';
const history = createHistory();
const middleware = routerMiddleware(history);
import '../css';
import reducers from "./reducers";
import purple from '@material-ui/core/colors/purple';
import {MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles'
import {blue, green, blueGrey, deepOrange } from '@material-ui/core/colors';

const theme = createMuiTheme({
  palette: {
    primary: {
      light: '#2e7031',
      main: '#43a047',
      dark: '#68b36b',
      contrastText: '#fff',
    },
    secondary: {
      light: '#b22a00',
      main: '#ff3d00',
      dark: '#ff6333',
      contrastText: '#000',
    },
    //primary: green,
    //secondary: deepOrange,
  },
});

const store = createStore(reducers, composeWithDevTools(applyMiddleware(middleware, thunk)));
ReactDOM.render(
  <MuiThemeProvider theme={theme}>
    <Provider store={store}>
      <Router history={history}>
        <Switch>
          <Route exact path='/' component={Init}/>
          <Route path='/Init' component={Init}/>
          <Route exact path='/tutor/:id' component={Tutor}/>
          <Route path='/main/:token' component={Main}/>
        </Switch>
      </Router>
    </Provider>
  </MuiThemeProvider>
  , document.getElementById('fieldToShow')
);
