import {fetchBody,secureFetchBody,status,getUrl,processing,processed,processingFailed } from './api';
import {loaded } from './regions';
import {loggedOut } from './session';
import {openSnack } from './snack';

export const load = () => {
  return dispatch => {
    dispatch(processing())
    return fetch(`${getUrl()}/regions/list`, 
      fetchBody({})
    )
      .then(status)
      .then(response => response.json())
      .then(json => {
        dispatch(loaded(json));
        dispatch(processed());
      })
      .catch(function(error) {  
        error.response.json().then(body=>{
          dispatch(openSnack(body.message||'Произошла системная ошибка'))
        })
        dispatch(processingFailed(error.message))
      });
  }
}
