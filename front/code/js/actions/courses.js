export const loaded = (list) => {
  return {
    type: "COURSES_LOADED",
    payload: list,
  }
};
export const setCurrent = (current_id) => {
  return {
    type: "COURSES_SET_CURRENT",
    payload: current_id,
  }
};
