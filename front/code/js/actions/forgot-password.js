export const changeUsername = (username) => {
  return {
    type: "FORGOT_USERNAME_CHANGED",
    payload: username 
  }
};
export const changePassword = (password) => {
  return {
    type: "FORGOT_PASSWORD_CHANGED",
    payload: password
  }
};
export const changePasswordConf = (password) => {
  return {
    type: "FORGOT_PASSWORD_CONF_CHANGED",
    payload: password
  }
};
export const changePin = (pin) => {
  return {
    type: "FORGOT_PIN_CHANGED",
    payload: pin,
  }
};
export const changeLang = (lang) => {
  return {
    type: "FORGOT_LANG_CHANGED",
    payload: lang,
  }
};
export const pinSent = () => {
  return {
    type: "FORGOT_PIN_SENT"
  }
};
export const approved = (data) => {
  return {
    type: "FORGOT_APPROVED",
    payload: data
  }
};
export const changeFailed = (message) => {
  return {
    type: "FORGOT_CHANGE_FAILED",
    payload: message
  }
};
export const clear = () => {
  return {
    type: "FORGOT_CLEAR",
  }
};
export const validateUsername = () => {
  return {
    type: "FORGOT_VALIDATE_USERNAME",
  }
}
export const validate = () => {
  return {
    type: "FORGOT_VALIDATE",
  }
}
