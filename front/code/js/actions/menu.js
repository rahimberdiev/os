export const changeCategory = (category) => {
  return {
    type: "CATEGORY_CHANGED",
    payload: category
  }
};
export const mOpen = (anchor) => {
  return {
    type: "MOPEN",
    payload: anchor
  }
};
export const mClose = (item) => {
  return {
    type: "MCLOSE",
    payload: item
  }
};
export const pOpen = (anchor) => {
  return {
    type: "POPEN",
    payload: anchor
  }
};
export const pClose = (item) => {
  return {
    type: "PCLOSE",
    payload: item
  }
};
export const sidenavOpen = () => {
  return {
    type: "SIDENAV_OPEN"
  }
};
export const sidenavClose = () => {
  return {
    type: "SIDENAV_CLOSE",
  }
};
