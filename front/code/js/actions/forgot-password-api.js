import {fetchBody,secureFetchBody,status,getUrl,processing,processed,processingFailed } from './api';
import {pinSent,changeFailed,approved,validate,validateUsername } from './forgot-password';
import {openSnack } from './snack';

import {loggedIn} from './session';

export const change= (username,lang) => {
  return dispatch => {
    if(!username){
      dispatch(validateUsername());
      return;
    }
    dispatch(processing())
    return fetch(`${getUrl()}/sessions/change_password`, 
      fetchBody({
        username,
        lang,
      })
    )
      .then(status)
      .then(response => response.json())
      .then(json => {
        dispatch(pinSent(json));
        dispatch(processed())
      })
      .catch(function(error) {  
        error.response.json().then(body=>{
          dispatch(changeFailed(body.message||'Произошла системная ошибка'))
          dispatch(openSnack(body.message||'Произошла системная ошибка'))
        })
        dispatch(processingFailed())
      });
  }
}
export const approve = (username,password,password_conf,pin,lang) => {
  return dispatch => {
    if(!password||password!=password_conf){
      dispatch(validate());
      return;
    }
    dispatch(processing())
    return fetch(`${getUrl()}/sessions/approve`, 
      fetchBody({
        pin,
        username,
        password,
        lang,
      })
    )
      .then(status)
      .then(response => response.json())
      .then(json => {
        dispatch(approved(json));
        dispatch(processed())
      })
      .catch(function(error) {  
        error.response.json().then(body=>{
          dispatch(changeFailed(body.message||'Произошла системная ошибка'))
          dispatch(openSnack(body.message||'Произошла системная ошибка'))
        })
        dispatch(processingFailed())
      });
  }
}
