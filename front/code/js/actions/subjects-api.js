import {fetchBody,secureFetchBody,status,getUrl,processing,processed,processingFailed } from './api';
import {loaded } from './subjects';
import {openSnack } from './snack';
import {loggedOut } from './session';

export const load = (token) => {
  return dispatch => {
    dispatch(processing())
    return fetch(`${getUrl()}/subjects/list`, 
      secureFetchBody(token,{})
    )
      .then(status)
      .then(response => response.json())
      .then(json => {
        dispatch(loaded(json));
        dispatch(processed());
      })
      .catch(function(error) {  
        if(error.message=='Unauthorized'){
          dispatch(openSnack('Ошибка авторизации'))
          return dispatch(loggedOut())
        }
        error.response.json().then(body=>{
          dispatch(openSnack(body.message||'Произошла системная ошибка'))
        })
        dispatch(processingFailed(error.message))
      });
  }
}
