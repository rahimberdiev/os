export const open = () => {
  return {
    type: "ACCESS_INFO_OPEN",
  }
};
export const close = () => {
  return {
    type: "ACCESS_INFO_CLOSE",
  }
};
