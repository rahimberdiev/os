const headers = {
  'Accept': 'application/json',
  'Content-Type': 'application/json'
}

//export const getUrl = () => {return "http://192.168.43.187:3000"}
//export const getUrl = () => {return "http://192.168.2.237:3000/api"}
export const getUrl = () => {return "http://176.126.167.36/api"}

export const getRes = (path) => {return path}
export const fetchBody = (json) => { 
  return {
    method: 'POST',
    headers: headers,
    mode: 'cors',
    cache: 'default',
    body: JSON.stringify(json)
  };
}
export const secureFetchBody = (token, json,method='POST')=>{
  var secureHeaders = Object.assign({},headers,{'Authorization': "Token token="+token});
  return {
    method: method,
    headers: secureHeaders,
    mode: 'cors',
    cache: 'default',
    body: JSON.stringify(json)
  };
}
export const status = (response) => {  
  if (response.status >= 200 && response.status < 300) {  
    return Promise.resolve(response)  
  }else if(response.status==401){
    return Promise.reject({message: 'Unauthorized',response}) 
  }else {  
    return Promise.reject({message: 'Other',response})  
  }  
}
export const processing = () => {
  return {
    type: "PROCESSING"
  }
};
export const processed = () => {
  return {
    type: "PROCESSED",
  }
};
export const processingFailed = (error_message) => {
  return {
    type: "PROCESSING_FAILED",
    payload: error_message,
  }
};
