export const openSnack = (message) => {
  return {
    type: "SNACK_OPEN",
    payload: message,
  }
};
export const closeSnack = () => {
  return {
    type: "SNACK_CLOSE",
  }
};
