export const openDialog = (content) => {
  return {
    type: "INIT_DIALOG_OPEN",
    payload: content,
  }
};
export const closeDialog = () => {
  return {
    type: "INIT_DIALOG_CLOSE",
  }
};
export const changeLang = (lang) => {
  return {
    type: "INIT_LANG_CHANGED",
    payload: lang
  }
};
