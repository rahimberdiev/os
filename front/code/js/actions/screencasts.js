export const loaded = (data) => {
  return {
    type: "SCREENCASTS_LOADED",
    payload: data,
  }
};
export const activated= (data) => {
  return {
    type: "SCREENCASTS_ACTIVATED",
    payload: data,
  }
};

export const nextSC= (index) => {
  return {
    type: "SCREENCASTS_NEXT_SC",
    payload: index, 
  }
};
export const prevSC= (index) => {
  return {
    type: "SCREENCASTS_PREV_SC",
    payload: index,
  }
};
