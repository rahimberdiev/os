export const loaded = (list) => {
  return {
    type: "PROBS_LOADED",
    payload: list,
  }
};
