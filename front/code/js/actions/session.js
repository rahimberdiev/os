export const changeUsername = (username) => {
  var un = username.replace(/\D/g,'');
  if(un.length>0 && un[0]!='0')
    un = '0'+un
  if(un.length>10) 
    un = un.substring(0,10)
  return {
    type: "SESSION_USERNAME_CHANGED",
    payload: un 
  }
};
export const changePassword = (password) => {
  return {
    type: "SESSION_PASSWORD_CHANGED",
    payload: password
  }
};
export const changeLang= (lang) => {
  return {
    type: "SESSION_LANG_CHANGED",
    payload: lang
  }
};
export const loggedIn = (data) => {
  return {
    type: "SESSION_LOGGED_IN",
    payload: data
  }
};
export const loginFailed = (message) => {
  return {
    type: "SESSION_LOGIN_FAILED",
    payload: message
  }
};
export const loggedOut = () => {
  return {
    type: "SESSION_LOGGED_OUT"
  }
};
export const validate = () => {
  return {
    type: "SESSION_VALIDATE",
  }
}
