import {fetchBody,secureFetchBody,status,getUrl,processing,processed,processingFailed } from './api';
import {loaded, activated } from './screencasts';
import {loggedOut } from './session';
import {openSnack } from './snack';

export const load = (token,section_id) => {
  return dispatch => {
    dispatch(processing())
    return fetch(`${getUrl()}/screencasts/list`, 
      secureFetchBody(token,{section_id})
    )
      .then(status)
      .then(response => response.json())
      .then(json => {
        dispatch(loaded(json));
        dispatch(processed())
      })
      .catch(function(error) {  
        if(error.message=='Unauthorized'){
          dispatch(openSnack('Ошибка авторизации'))
          return dispatch(loggedOut())
        }
        error.response.json().then(body=>{
          dispatch(openSnack(body.message||'Произошла системная ошибка'))
        })
        dispatch(processingFailed(error.message))
      });
  }
}

export const activate = (token,section_id,sc_id) => {
  return dispatch => {
    dispatch(processing())
    return fetch(`${getUrl()}/screencasts/activate`, 
      secureFetchBody(token,{section_id,sc_id})
    )
      .then(status)
      .then(response => response.json())
      .then(json => {
        dispatch(activated(json));
        dispatch(processed());
      })
      .catch(function(error) {  
        if(error.message=='Unauthorized'){
          dispatch(openSnack('Ошибка авторизации'))
          return dispatch(loggedOut())
        }
        error.response.json().then(body=>{
          dispatch(openSnack(body.message||'Произошла системная ошибка'))
        })
        dispatch(processingFailed(error.message))
      });
  }
}
