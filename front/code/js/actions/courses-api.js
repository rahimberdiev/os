import {fetchBody,secureFetchBody,status,getUrl,processing,processingFailed,processed } from './api';
import {loaded } from './courses';
import {openSnack } from './snack';
import {loggedOut } from './session';

export const load = (token,subject_id) => {
  return dispatch => {
    dispatch(processing())
    return fetch(`${getUrl()}/courses/list`, 
      secureFetchBody(token,{subject_id: subject_id})
    )
      .then(status)
      .then(response => response.json())
      .then(json => {
        dispatch(loaded(json));
        dispatch(processed());
      })
      .catch(function(error) {  
        if(error.message=='Unauthorized'){
          dispatch(openSnack('Ошибка авторизации'))
          return dispatch(loggedOut())
        }
        error.response.json().then(body=>{
          dispatch(openSnack(body.message||'Произошла системная ошибка'))
        })
        dispatch(processingFailed(error.message))
      });
  }
}
