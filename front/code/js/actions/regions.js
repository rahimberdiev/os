export const loaded = (list) => {
  return {
    type: "REGIONS_LOADED",
    payload: list,
  }
};
