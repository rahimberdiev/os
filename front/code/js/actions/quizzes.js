export const loaded = (list) => {
  return {
    type: "QUIZZES_LOADED",
    payload: list,
  }
};
export const open = () => {
  return {
    type: "QUIZZES_OPEN"
  }
}
export const close = () => {
  return {
    type: "QUIZZES_CLOSE"
  }
}
export const moveNext = () =>{
  return {
    type: "QUIZZES_MOVE_NEXT"
  }
}
export const moveBack = () =>{
  return {
    type: "QUIZZES_MOVE_BACK"
  }
}
export const select = (quiz_id,answer) =>{
  return {
    type: "QUIZZES_SELECT",
    payload:{quiz_id,answer}
  }
}
export const checked = (result) =>{
  return {
    type: "QUIZZES_CHECKED",
    payload:result
  }
}
export const expand = (expand) =>{
  return {
    type: "QUIZZES_EXPANDED",
    payload:expand
  }
}
