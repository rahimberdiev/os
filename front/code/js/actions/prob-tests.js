export const loaded = (list) => {
  return {
    type: "PROB_TESTS_LOADED",
    payload: list,
  }
};
export const restartProb = () => {
  return {
    type: "PROB_TESTS_RESTART"
  }
};
export const select = (prob_test_id,answer) =>{
  return {
    type: "PROB_TESTS_SELECT",
    payload:{prob_test_id,answer}
  }
}
export const checked = (result) =>{
  return {
    type: "PROB_TESTS_CHECKED",
    payload:result
  }
}
export const tic = () => {
  return {
    type: "PROB_TESTS_TIC",
  }
};
