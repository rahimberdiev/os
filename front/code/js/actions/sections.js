export const loaded = (data) => {
  return {
    type: "SECTIONS_LOADED",
    payload: data,
  }
};
export const select = (selected_id) => {
  return {
    type: "SECTIONS_SELECTED",
    payload: selected_id,
  }
};
export const moved= (data) => {
  return {
    type: "SECTIONS_MOVED",
    payload: data
  }
};
