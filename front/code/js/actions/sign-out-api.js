import {fetchBody,secureFetchBody,status,getUrl,processing,processed,processingFailed } from './api';
import {signedOut,failedToSignOut,validate,approved } from './sign-out';
import {openSnack } from './snack';

import {loggedIn} from './session';

export const signOut= (username,fullname,lang, password,password_conf,agreed,region,city) => {
  return dispatch => {
    if(!username||username.trim().length!=10||!fullname||!password||password!=password_conf||!agreed||region==-1){
      if(!agreed)
        dispatch(openSnack("Регистрация невозможна без согласия с условиями оферты"))
      dispatch(validate());
      return;
    }
    dispatch(processing())
    return fetch(`${getUrl()}/signout/create`, 
      fetchBody({
        username,
        fullname,
        lang,
        password,
        agreed,
        region,
        city,
      })
    )
      .then(status)
      .then(response => response.json())
      .then(json => {
        dispatch(signedOut(json));
        dispatch(processed())
      })
      .catch(function(error) {  
        error.response.json().then(body=>{
          dispatch(failedToSignOut(body.message||'Произошла системная ошибка'))
          dispatch(openSnack(body.message||'Произошла системная ошибка'))
        })
        dispatch(processingFailed())
      });
  }
}
export const approve = (username,code,lang) => {
  return dispatch => {
    dispatch(processing())
    return fetch(`${getUrl()}/signout/approve`, 
      fetchBody({
        username,
        code,
        lang,
      })
    )
      .then(status)
      .then(response => response.json())
      .then(json => {
        dispatch(approved(json));
        dispatch(processed())
      })
      .catch(function(error) {  
        error.response.json().then(body=>{
          dispatch(failedToSignOut(body.message||'Произошла системная ошибка'))
          dispatch(openSnack(body.message||'Произошла системная ошибка'))
        })
        dispatch(processingFailed())
      });
  }
}
