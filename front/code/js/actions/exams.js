export const loaded = (list) => {
  return {
    type: "EXAMS_LOADED",
    payload: list,
  }
};
export const moveNext = () =>{
  return {
    type: "EXAMS_MOVE_NEXT"
  }
}
export const moveBack = () =>{
  return {
    type: "EXAMS_MOVE_BACK"
  }
}
export const select = (exam_id,answer) =>{
  return {
    type: "EXAMS_SELECT",
    payload:{exam_id,answer}
  }
}
export const checked = (result) =>{
  return {
    type: "EXAMS_CHECKED",
    payload:result
  }
}
export const expand = (exam_id) =>{
  return {
    type: "EXAMS_EXPANDED",
    payload:exam_id
  }
}
