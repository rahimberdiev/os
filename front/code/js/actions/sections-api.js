import {fetchBody,secureFetchBody,status,getUrl,processing,processed,processingFailed } from './api';
import {loaded, moved,activated } from './sections';
import {loggedOut } from './session';
import {openSnack } from './snack';

export const load = (token,course_id) => {
  return dispatch => {
    dispatch(processing())
    return fetch(`${getUrl()}/sections/list`, 
      secureFetchBody(token,{course_id: course_id})
    )
      .then(status)
      .then(response => response.json())
      .then(json => {
        dispatch(loaded(json));
        dispatch(processed())
      })
      .catch(function(error) {  
        if(error.message=='Unauthorized'){
          dispatch(openSnack('Ошибка авторизации'))
          return dispatch(loggedOut())
        }
        error.response.json().then(body=>{
          dispatch(openSnack(body.message||'Произошла системная ошибка'))
        })
        dispatch(processingFailed(error.message))
      });
  }
}

export const move = (token,course_id) => {
  return dispatch => {
    dispatch(processing())
    return fetch(`${getUrl()}/sections/move`, 
      secureFetchBody(token,{course_id: course_id})
    )
      .then(status)
      .then(response => response.json())
      .then(json => {
        dispatch(moved(json));
        dispatch(processed());
      })
      .catch(function(error) {  
        if(error.message=='Unauthorized'){
          dispatch(openSnack('Ошибка авторизации'))
          return dispatch(loggedOut())
        }
        error.response.json().then(body=>{
          dispatch(openSnack(body.message||'Произошла системная ошибка'))
        })
        dispatch(processingFailed(error.message))
      });
  }
}

