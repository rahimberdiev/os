import {fetchBody,secureFetchBody,status,getUrl,processing,processed,processingFailed } from './api';
import {probsLoaded, loaded, checked } from './prob-tests';
import {loggedOut } from './session';
import {openSnack } from './snack';


export const loadProbs = (token) => {
  return dispatch => {
    dispatch(processing())
    return fetch(`${getUrl()}/probs/probs_list`, 
      secureFetchBody(token,{})
    )
      .then(status)
      .then(response => response.json())
      .then(json => {
        dispatch(probsLoaded(json));
        dispatch(processed());
      })
      .catch(function(error) {  
        if(error.message=='Unauthorized'){
          dispatch(openSnack('Ошибка авторизации'))
          return dispatch(loggedOut())
        }
        error.response.json().then(body=>{
          dispatch(openSnack(body.message||'Произошла системная ошибка'))
        })
        dispatch(processingFailed(error.message))
      });
  }
}
export const load = (token,prob_id) => {
  return dispatch => {
    dispatch(processing())
    return fetch(`${getUrl()}/probs/list`, 
      secureFetchBody(token,{testing_id: prob_id})
    )
      .then(status)
      .then(response => response.json())
      .then(json => {
        dispatch(loaded(json));
        dispatch(processed());
      })
      .catch(function(error) {  
        if(error.message=='Unauthorized'){
          dispatch(openSnack('Ошибка авторизации'))
          return dispatch(loggedOut())
        }
        error.response.json().then(body=>{
          dispatch(openSnack(body.message||'Произошла системная ошибка'))
        })
        dispatch(processingFailed(error.message))
      });
  }
}
export const check = (token,prob_id,work) => {
  return dispatch => {
    dispatch(processing())
    return fetch(`${getUrl()}/probs/check`, 
      secureFetchBody(token,{testing_id: prob_id, work})
    )
      .then(status)
      .then(response => response.json())
      .then(json => {
        dispatch(checked(json));
        dispatch(processed());
      })
      .catch(function(error) {  
        if(error.message=='Unauthorized'){
          dispatch(openSnack('Ошибка авторизации'))
          return dispatch(loggedOut())
        }
        error.response.json().then(body=>{
          dispatch(openSnack(body.message||'Произошла системная ошибка'))
        })
        dispatch(processingFailed(error.message))
      });
  }
}
