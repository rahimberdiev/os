export const changeUsername = (username) => {
  return {
    type: "SIGN_OUT_USERNAME_CHANGED",
    payload: username
  }
};
export const changeFullname = (fullname) => {
  return {
    type: "SIGN_OUT_FULLNAME_CHANGED",
    payload: fullname
  }
};
export const changeLang = (lang) => {
  return {
    type: "SIGN_OUT_LANG_CHANGED",
    payload: lang
  }
};
export const changeRegion = (region) => {
  return {
    type: "SIGN_OUT_REGION_CHANGED",
    payload: region
  }
};
export const changeCity = (city) => {
  return {
    type: "SIGN_OUT_CITY_CHANGED",
    payload: city 
  }
};
export const changePassword = (password) => {
  return {
    type: "SIGN_OUT_PASSWORD_CHANGED",
    payload: password
  }
};
export const changePasswordConf = (password) => {
  return {
    type: "SIGN_OUT_PASSWORD_CONF_CHANGED",
    payload: password
  }
};

export const changeStep= (step) => {
  return {
    type: "SIGN_OUT_STEP_CHANGED",
    payload: step
  }
};
export const changeCode= (code) => {
  return {
    type: "SIGN_OUT_CODE_CHANGED",
    payload: code
  }
};
export const setAgreed = (agreed) => {
  return {
    type: "SIGN_OUT_AGREED_CHANGED",
    payload: agreed, 
  }
};
export const signedOut= (data) => {
  return {
    type: "SIGNED_OUT",
    payload: data
  }
};
export const clear= () => {
  return {
    type: "SIGN_OUT_CLEAR",
  }
};
export const approved = (data) => {
  return {
    type: "SIGN_OUT_APPROVED",
    payload: data
  }
};
export const failedToSignOut= (error) => {
  return {
    type: "SIGN_OUT_FAILED",
    payload: error
  }
};
export const validate = () => {
  return {
    type: "SIGN_OUT_VALIDATE",
  }
}
