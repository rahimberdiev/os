export const loaded = (list) => {
  return {
    type: "SUBJECTS_LOADED",
    payload: list,
  }
};
