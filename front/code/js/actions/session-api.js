import {fetchBody,secureFetchBody,status,getUrl,processing,processed,processingFailed } from './api';
import {loggedIn,loginFailed,validate,loggedOut } from './session';
import {openSnack } from './snack';

export const login = (username, password, lang) => {
  return dispatch => {
    if(!username||username.trim().length!=10||!password){
      dispatch(validate());
      return;
    }
    dispatch(processing())
    return fetch(`${getUrl()}/sessions/create`, 
      fetchBody({
        username,
        password,
        lang,
      })
    )
      .then(status)
      .then(response => response.json())
      .then(json => {
        dispatch(loggedIn(json));
        dispatch(processed())
      })
      .catch(function(error) {  
        error.response.json().then(body=>{
          dispatch(openSnack(body.message||'Произошла системная ошибка'))
          dispatch(loginFailed(body.message||'Произошла системная ошибка'))
        })
        dispatch(processingFailed(error.message))
      });
  }
}
export const refreshSession = (token) => {
  return dispatch => {
    dispatch(processing())
    return fetch(`${getUrl()}/sessions/refresh_session`, 
      secureFetchBody(token,{}))
      .then(status)
      .then(response => response.json())
      .then(json => {
        dispatch(loggedIn(json));
        dispatch(processed())
      })
      .catch(function(error) {  
        error.response.json().then(body=>{
          dispatch(openSnack(body.message||'Произошла системная ошибка'))
          dispatch(loginFailed(body.message||'Произошла системная ошибка'))
        })
        dispatch(processingFailed(error.message))
      });
  }
}

export const logout = (token) => {
  return dispatch => {
    dispatch(processing())
    return fetch(`${getUrl()}/sessions/destroy`, 
      secureFetchBody(token,{},'DELETE'))
      .then(status)
      .then(response => response.json())
      .then(json => {
        dispatch(loggedOut())
        dispatch(processed())
      })
      .catch(function(error) {  
        if(error.message=='Unauthorized'){
          dispatch(openSnack(error.message))
          return dispatch(loggedOut())
        }
        error.response.json().then(body=>{
          dispatch(openSnack(body.message||'Произошла системная ошибка'))
        })
        dispatch(processingFailed(error.message))
      });
  }
}
