import {fetchBody,secureFetchBody,status,getUrl,processing,processed,processingFailed } from './api';
import {loaded, checked } from './quizzes';
import {loggedOut } from './session';
import {openSnack } from './snack';

export const load = (token,sc_id) => {
  return dispatch => {
    dispatch(processing())
    return fetch(`${getUrl()}/quizzes/list`, 
      secureFetchBody(token,{sc_id})
    )
      .then(status)
      .then(response => response.json())
      .then(json => {
        dispatch(loaded(json));
        dispatch(processed());
      })
      .catch(function(error) {  
        if(error.message=='Unauthorized'){
          dispatch(openSnack('Ошибка авторизации'))
          return dispatch(loggedOut())
        }

        error.response.json().then(body=>{
          dispatch(openSnack(body.message||'Произошла системная ошибка'))
        })
        dispatch(processingFailed(error.message))
      });
  }
}
export const check = (token,sc_id,work) => {
  return dispatch => {
    dispatch(processing())
    return fetch(`${getUrl()}/quizzes/check`, 
      secureFetchBody(token,{sc_id, work})
    )
      .then(status)
      .then(response => response.json())
      .then(json => {
        dispatch(checked(json));
        dispatch(processed());
      })
      .catch(function(error) {  
        if(error.message=='Unauthorized'){
          dispatch(openSnack('Ошибка авторизации'))
          return dispatch(loggedOut())
        }

        error.response.json().then(body=>{
          dispatch(openSnack(body.message||'Произошла системная ошибка'))
        })
        dispatch(processingFailed(error.message))
      });
  }
}
