Rails.application.routes.draw do
  devise_for :admins
  root to: "subjects#index"

  resources :lectors 

  resources :users do 
    resources :user_courses, only: [:index,:new,:create,:destroy]
  end
  get "user/edit_pwd/:id" ,to: "users#edit_pwd", as: "edit_user_pwd"
  resources :user_courses, only: [:show, :edit, :update]


  resources :subjects do 
    resources :courses, only: [:index,:new,:create, :destroy] 
  end
  get '/add_lector/:course_id/:lector_id', to: 'courses#add_lector', as: 'add_lector'
  get '/remove_lector/:course_id/:lector_id', to: 'courses#remove_lector', as: 'remove_lector'
  resources :courses, only: [:show, :edit, :update] do
    resources :sections, only: [:index,:new, :create, :destroy]
  end
  resources :sections, only: [:show, :edit, :update] do
    resources :screencasts, only: [:index,:new, :create, :destroy]
    resources :exams, only: [:index,:new, :create, :destroy]
  end
  resources :screencasts, only: [:show, :edit, :update] do 
    resources :quizzes, only: [:index,:new, :create, :destroy]
  end
  resources :quizzes, only: [:show, :edit, :update] do
    resources :options, only: [:index,:new, :create, :destroy]
    resources :imgs, only: [:index,:new, :create, :destroy]
  end
  resources :options, only: [:show, :edit, :update]
  resources :imgs, only: [:show, :edit, :update]

  resources :exams, only: [:show, :edit, :update] do
    resources :answers, only: [:index,:new, :create, :destroy]
    resources :pics, only: [:index,:new, :create, :destroy]
  end
  resources :answers, only: [:show, :edit, :update]
  resources :pics, only: [:show, :edit, :update]


end
