class CreateUsers < ActiveRecord::Migration[5.1]
  def change
    create_table :users do |t|
      t.integer :lang
      t.string :username
      t.string :password_digest
      t.string :fullname
      t.string :email
      t.string :phone
      t.integer :region
      t.integer :district
      t.string :school
      t.attachment :photo

      t.string :token
      t.string :pin
      t.datetime :token_created_at
      t.index [:token,:token_created_at]

      t.timestamps
    end
  end
end
