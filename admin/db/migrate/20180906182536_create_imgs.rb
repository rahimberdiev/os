class CreateImgs < ActiveRecord::Migration[5.1]
  def change
    create_table :imgs do |t|
      t.references :quiz, foreign_key: true
      t.attachment :img
      t.string :title
      t.timestamps
    end
  end
end
