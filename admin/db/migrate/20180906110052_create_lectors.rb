class CreateLectors < ActiveRecord::Migration[5.1]
  def change
    create_table :lectors do |t|
      t.string :fullname
      t.string :level
      t.string :degree
      t.attachment :photo
      t.timestamps
    end
  end
end
