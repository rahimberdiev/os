class Screencast < ApplicationRecord
  has_attached_file :logo, styles: { medium: "300x300>", thumb: "100x100>" }, default_url: "/images/:style/missing.png"
  validates_attachment_content_type :logo, content_type: /\Aimage\/.*\z/
  has_attached_file :video
  validates_attachment_content_type :video, content_type: /\Avideo\/.*\z/
  belongs_to :section
  has_many :quizzes
end
