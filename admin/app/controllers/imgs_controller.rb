class ImgsController < ApplicationController
  before_action :set_img, only: [:show, :edit, :update, :destroy]
  before_action :set_quiz, only: [:index,:new, :create, :destroy]

  # GET /imgs
  # GET /imgs.json
  def index
    @imgs = @quiz.imgs
  end

  # GET /imgs/1
  # GET /imgs/1.json
  def show
  end

  # GET /imgs/new
  def new
    @img = @quiz.imgs.new
  end

  # GET /imgs/1/edit
  def edit
  end

  # POST /imgs
  # POST /imgs.json
  def create
    @img = @quiz.imgs.new(img_params)

    respond_to do |format|
      if @img.save
        format.html { redirect_to @img.quiz, notice: 'Img was successfully created.' }
      else
        format.html { render :new }
      end
    end
  end

  # PATCH/PUT /imgs/1
  # PATCH/PUT /imgs/1.json
  def update
    respond_to do |format|
      if @img.update(img_params)
        format.html { redirect_to @img.quiz, notice: 'Img was successfully updated.' }
      else
        format.html { render :edit }
      end
    end
  end

  # DELETE /imgs/1
  # DELETE /imgs/1.json
  def destroy
    @img.destroy
    respond_to do |format|
      format.html { redirect_to @quiz, notice: 'Img was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_img
      @img = Img.find(params[:id])
    end
    def set_quiz
      @quiz = Quiz.find(params[:quiz_id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def img_params
      params.require(:img).permit(:img,:title)
    end
end
