class PicsController < ApplicationController
  before_action :set_pic, only: [:show, :edit, :update, :destroy]
  before_action :set_exam, only: [:index,:new, :create, :destroy]

  # GET /pics
  # GET /pics.json
  def index
    @pics = @exam.pics
  end

  # GET /pics/1
  # GET /pics/1.json
  def show
  end

  # GET /pics/new
  def new
    @pic = @exam.pics.new
  end

  # GET /pics/1/edit
  def edit
  end

  # POST /pics
  # POST /pics.json
  def create
    @pic = @exam.pics.new(pic_params)

    respond_to do |format|
      if @pic.save
        format.html { redirect_to @pic.exam, notice: 'Pic was successfully created.' }
      else
        format.html { render :new }
      end
    end
  end

  # PATCH/PUT /pics/1
  # PATCH/PUT /pics/1.json
  def update
    respond_to do |format|
      if @pic.update(pic_params)
        format.html { redirect_to @pic.exam, notice: 'Pic was successfully updated.' }
      else
        format.html { render :edit }
      end
    end
  end

  # DELETE /pics/1
  # DELETE /pics/1.json
  def destroy
    @pic.destroy
    respond_to do |format|
      format.html { redirect_to @exam, notice: 'Pic was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_pic
      @pic = Pic.find(params[:id])
    end
    def set_exam
      @exam = Exam.find(params[:exam_id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def pic_params
      params.require(:pic).permit(:img,:title)
    end
end
