class ScreencastsController < ApplicationController
  before_action :set_screencast, only: [:show, :edit, :update, :destroy]
  before_action :set_section, only: [:index,:new, :create, :destroy]

  # GET /screencasts
  # GET /screencasts.json
  def index
    @screencasts = @section.screencasts
  end

  # GET /screencasts/1
  # GET /screencasts/1.json
  def show
  end

  # GET /screencasts/new
  def new
    @screencast = @section.screencasts.new
    @screencast.no = @section.screencasts.maximum(:no) ? @section.screencasts.maximum(:no)+1 : 1 
  end

  # GET /screencasts/1/edit
  def edit
  end

  # POST /screencasts
  # POST /screencasts.json
  def create
    @screencast = @section.screencasts.new(screencast_params)

    respond_to do |format|
      if @screencast.save
        format.html { redirect_to @screencast, notice: 'Screencast was successfully created.' }
      else
        format.html { render :new }
      end
    end
  end

  # PATCH/PUT /screencasts/1
  # PATCH/PUT /screencasts/1.json
  def update
    respond_to do |format|
      if @screencast.update(screencast_params)
        format.html { redirect_to @screencast, notice: 'Screencast was successfully updated.' }
      else
        format.html { render :edit }
      end
    end
  end

  # DELETE /screencasts/1
  # DELETE /screencasts/1.json
  def destroy
    @screencast.destroy
    respond_to do |format|
      format.html { redirect_to @section, notice: 'Screencast was successfully destroyed.' }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_screencast
      @screencast = Screencast.find(params[:id])
    end

    def set_section
      @section = Section.find(params[:section_id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def screencast_params
      params.require(:screencast).permit(:no, :name, :logo, :video, :section_id, :q_count, :free)
    end
end
