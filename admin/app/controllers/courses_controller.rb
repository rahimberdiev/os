class CoursesController < ApplicationController
  before_action :set_course, only: [:show, :edit, :update, :destroy]
  before_action :set_subject, only: [:index,:new, :create, :destroy]

  def index
    @courses = @subject.courses
  end

  def show
    @lectors = Lector.all
  end

  def new
    @course = @subject.courses.new
    @course.no = @subject.courses.maximum(:no) ? @subject.courses.maximum(:no)+1 : 1
  end

  def edit
  end

  def create
    @course = @subject.courses.new(course_params)

    respond_to do |format|
      if @course.save
        format.html { redirect_to @course, notice: 'Course was successfully created.' }
      else
        format.html { render :new }
      end
    end
  end
  def add_lector 
    course = Course.find(params[:course_id])
    lector = Lector.find(params[:lector_id])
    course.lectors<<lector
    course.save
    respond_to do |format|
      format.html { redirect_to course }
    end
  end
  def remove_lector 
    course = Course.find(params[:course_id])
    lector = Lector.find(params[:lector_id])
    course.lectors.destroy(params[:lector_id])
    course.save
    respond_to do |format|
      format.html { redirect_to course }
    end
  end

  # PATCH/PUT /courses/1
  # PATCH/PUT /courses/1.json
  def update
    respond_to do |format|
      if @course.update(course_params)
        format.html { redirect_to @course, notice: 'Course was successfully updated.' }
      else
        format.html { render :edit }
      end
    end
  end

  # DELETE /courses/1
  # DELETE /courses/1.json
  def destroy
    @course.destroy
    respond_to do |format|
      format.html { redirect_to @subject, notice: 'Course was successfully destroyed.' }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_course
      @course = Course.find(params[:id])
    end
    def set_subject
      @subject = Subject.find(params[:subject_id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def course_params
      params.require(:course).permit(:no, :name, :description, :logo, :subject_id, :level, :lector)
    end
end
